"""Implement map, filter, reduce in python."""

# %%


def map1(f, lst):
    """Implement map in python."""
    res = []
    for i in lst:
        res.append(f(i))
    return res


def square_num(n):
    """Square a given number."""
    return n * n


map1(square_num, [1, 2, 3, 4])
map1(square_num, [])
map1(square_num, [2])

# %%


def filter1(f, lst):
    """Implement filter in python."""
    res = []
    for i in lst:
        if f(i):
            res.append(i)
    return res


def even(n):
    """Find whether the given number is even or not."""
    if n % 2 == 0:
        return True


filter1(even, [1, 2, 3, 4])
filter1(even, [])
filter1(even, [1])
filter1(even, [2])

# %%


def reduce1(f, init, lst):
    """Implement reduce in python."""
    res = init
    for i in lst:
        res = f(res, i)
    return res


def add(a, b):
    """Add the given two numbers."""
    return a + b


reduce1(add, 0, [1, 2])
reduce1(add, 0, [1, 2, 3, 4])
reduce1(add, 0, [])
reduce1(add, 0, [2])

# %%
