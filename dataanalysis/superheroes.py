"""Explore Superheroes dataset."""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# %%
# load the dataset
data = pd.read_csv('~/data/heroes_information.csv')
data.head()
data.shape

# %%
data .info()
data.columns
data.describe()

# %%
# check for nulls
data.isnull().any()
data.isnull().sum()

# %%
# Dealing with nulls
data1 = data.dropna(subset=['Weight', 'Publisher'])
data.head()
data1.isnull().any()

# ------------------------------------------------------------------

# %%
# What are the total no of superheroes
data1['name'].nunique()

# %%
# average height of superheroes
data1['Height'].mean()

# %%
# average weight of superheroes
data1['Weight'].mean()

# %%
# no of heroes with grey colored eyes
data1[data1['Eye color'] == 'grey'].shape
# there are only 6 superheroes with grey colored eyes

# %%
# no of superheroes with no hair
data1[data1['Hair color'] == 'No Hair'].shape

# %%
# plot indicating the no of superheroes in  each gender
sh_g = data.groupby('Gender').size()
sh_g.plot(kind='bar')

# %%
# in which comic there are more no of superheroes
sh = data1.groupby(['Publisher'])['name'].nunique()
sh1 = sh.sort_values(ascending=False)
sh1.plot(kind='bar')

# %%
# barplot indicating the no of publications of each publisher
p_p = data1.groupby(['Publisher']).size()
p_p.plot(kind='bar')

# %%
# which superhero has got highest no of publications in both male and females
g_h = data1.groupby(['Gender', 'name']).size().sort_values(ascending=False)
g_h.groupby('Gender').apply(lambda x: x.sort_values(ascending=False).head(1))


# %%
# find the percentage of superheroes in each gender whose height is less
# than the  mean height in their respective genders


def less_than_mean(data, variable, column):
    """Find percentage."""
    res = []
    res1 = []
    u_values = data[column].unique()
    for i in u_values:
        d1 = data[data[column] == i]
        d2 = d1[d1[variable] < d1[variable].mean()]
        p = d2.shape[0]/d1.shape[0] * 100
        res1.append(i)
        res.append(p)
    return res1, res


less_than_mean(data1, 'Height', 'Gender')

# %%
# what are the different hair colors
h_u = data1['Hair color'].unique()
h_u
h_u1 = []
for i in h_u:
    a = i.split('/')
    h_u1.append(a)
h_u1


def flattening(lst):
    """Flatten a nested list structure."""
    res = []
    if len(lst) != 0:
        for e in lst:
            if type(e) == list:
                res.extend(flattening(e))
            else:
                res.append(e)
        return res
    return None


h_u2 = flattening(h_u1)
pd.Series(h_u2).nunique()
# There are 32 different hair colors.

# %%
# what are the different eye colors
e_u = data1['Eye color'].unique()
e_u
e_u1 = []
for i in e_u:
    a = i.split('/')
    e_u1.append(a)
e_u1

e_u2 = flattening(e_u1)
pd.Series(e_u2).nunique()
# # There are 25 different eye colors.


# %%
# which publisher published which superheroes  comics highest
p1 = data1.groupby('Publisher')['name']
p1.apply(lambda x: x.sort_values(ascending=False).head(1))

# %%
# plot for top 20 Race types of super heroes
r = data['Race'].value_counts().head(10)
r.plot(kind='bar')

# %%
#  plot for Alignment of male and female super heroes
a1 = data['Gender'].unique()
plt.figure()
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.45,
                    wspace=0.4)
for i, j in zip(a1, [1, 2, 3]):
    ga = data1[data1['Gender'] == i]['Alignment'].value_counts()
    plt.subplot(3, 1, j)
    plt.plot(ga)
    plt.title(i)

# %%
# Different distributions
# Hair color, skin color and eye color distibution of Superheroes
cols = ['Hair color', 'Eye color', 'Skin color']
plt.figure(figsize=(20, 15))
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.35,
                    wspace=0.4)
for i, j in zip(cols, [1, 2, 3]):
    dt = data1.groupby(i).size()
    plt.subplot(3, 1, j)
    plt.plot(dt)
    plt.xticks(rotation=45)
    plt.title(i)

# %%
# distributions of skin color in human Race in male
d_h = (data1[np.all([data1['Race'] == 'Human', data1['Gender'] == 'Male'],
       axis=0)])
d_s = d_h['Skin color'].value_counts()
d_s.plot(kind='bar')

# %%
# Average no of superheroes in  any comic
a_s_h = data1.groupby('Publisher')
a_s_h1 = a_s_h['name'].apply(lambda x: x.nunique())
a_s_h1.values.mean()

# %%
# which color eyes most superheroes of human race have
data1[data1['Race'] == 'Human']['Skin color'].value_counts()

# %%
# top superheroes wrt height in each gender
t_h = data1.groupby('Gender')['Height']
t_h.apply(lambda x: x.sort_values(ascending=False).head(3))
