"""loading and checking nulls for ipl data set."""

# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/matches.csv')
data.head()
data.info()

# %%df.values
# drop umpire3 column
data.isnull().sum()
data = data.drop(['umpire3'], axis='columns')

# %%
# barplot for no of playerof match awards for each player
d1 = data['player_of_match'].value_counts().head(10)
plt.xticks(rotation=90)
plt.yticks([0, 2, 4, 6, 8, 10, 12, 14, 16, 18])
x = sns.barplot(x=d1.index, y=d1.values)

# %%
# total no of matches played by each team
match = data['team1'].value_counts() + data['team2'].value_counts()
# barplot for total no of matches played by each team
plt.xticks(rotation=90)
sns.barplot(x=match.index, y=match.values)

# %%
# total no of matches played by eact teeam in the year 2017
data2017 = data[data['season'] == 2017]
data2017['team1'].value_counts() + data2017['team2'].value_counts()

# %%
# no of matches won by each team
df = data['winner'].value_counts()
per = (df/match)*100
per

ipl = pd.DataFrame()
ipl['matches'] = match
ipl['matches_win'] = df
ipl['win_per'] = per
ipl.sort_values(by='win_per', ascending=False)


# %%
def first_played(df):
    """Function."""
    if df['toss_decision'] == 'bat':
        return df['toss_winner']
    else:
        if df['toss_winner'] != df['team1']:
            return df['team1']
        else:
            return df['team2']


data.apply(first_played, axis=1)
data1 = data
data1['bat_first_team'] = data.apply(first_played, axis=1)
data1.tail(25)


# %%
# add a column to indicate whether a winning team batted or fielded first
data1['winner_bat_field'] = (np.where(data1['bat_first_team'] ==
                                      data1['winner'], 'bat', 'field'))
data1.head(10)

# %%
# display win count when batting first vs fielding first_played
data1['winner_bat_field'].value_counts()

# %%
# calculate ratio of wins fielding first vs wins batting first_played
data1['winner_bat_field'].value_counts()/len(data1)

# %%
# plot for win count when batting first vs fileld first
x = data1['winner_bat_field'].value_counts()/len(data1)
sns.barplot(x=x.index, y=x.values)

# %%
# plot for win count when batting first vs fileld first for each team
count_teams = data1.groupby('winner')['winner_bat_field'].value_counts()
c1 = count_teams.index.get_level_values(0)
c2 = count_teams.index.get_level_values(1)
plt.xticks(rotation=90)
x = sns.barplot(x=c1, y=count_teams.values, hue=c2)

# ----------------------------------------------------------------------------
# %%
# no of times toss winners are the match winners
data[data['toss_winner'] == data['winner']].shape[0]

# %%
# toss decisions of each team
data.groupby('toss_winner')['toss_decision'].value_counts().unstack()

# %%
# in which city more ipl matches are held
data['city'].value_counts().head()

# %%
# which team won with highest runs difference
data['win_by_runs'].max()
data[data['win_by_runs'] == data['win_by_runs'].max()].groupby('winner').size()

# %%
# In which city each team won more matches
data.groupby('winner')['city'].value_counts()

# ----------------------------------------------------------------------

# %%
# win percentage of each team when played in mumbai
dm = data[data['city'] == 'Mumbai'].groupby('winner').size()
dm/data.groupby('winner').size() * 100

# %%
# top umpire
data.groupby('umpire1').size().sort_values(ascending=False).head()

# %%
# under whose empiring each team won more matches
data.groupby(['winner', 'umpire1']).size()

# %%
# highest run difference for each team
y1 = data.groupby('winner')['win_by_runs']
y1.apply(lambda x: x.sort_values(ascending=False).head(1))

# %%
# average  no of winbywickets for each team
w1 = data.groupby('winner')['win_by_wickets']
w1.apply(lambda x: x.mean())
