"""Deliveries file."""

import pandas as pd
data = pd.read_csv('~/data/deliveries.csv')
data.head()
data.isnull().any()
data.isnull().sum()
data.head(15)

# %%
# total no of matches played
data['match_id'].nunique()

# %%
# find the total no of teams
data['batting_team'].nunique()

# %%
# find the total no of players
data['batsman'].nunique()
p1 = (pd.concat((data['batsman'], data['non_striker'], data['bowler']),
      ignore_index=True))
p1.nunique()

# %%
# find the top 10 players who have scored the highest no of runs
(data.groupby('batsman')['total_runs'].sum().sort_values(ascending=False).
 head(10))

# ---------------------------------------------------------------------------

# %%
# Top 10 bowlers
data.groupby('bowler').size().sort_values(ascending=False).head(10)


# %%
# no of wickets taken by each bowler
w = data.groupby('bowler')['dismissal_kind'].value_counts().unstack()
w
w2 = (w[['bowled', 'caught', 'lbw', 'caught and bowled', 'stumped',
      'hit wicket']])
w2


def total(data):
    """Add elements in a row."""
    res = []
    for i in range(len(data)):
        res.append(data.iloc[i].sum())
    return res


w2['tot'] = total(w2)
w2['tot'].sort_values(ascending=False).head()


# %%
# batsman with highest runs(exclude extra runs)
data.groupby('batsman')['batsman_runs'].sum().sort_values(ascending=False)

# %%
# Bowlers with more wideruns
data.groupby('bowler')['wide_runs'].sum().sort_values(ascending=False).head(10)

# %%
# which dismissal_kind lead to highest players dismissals
data.groupby('dismissal_kind').size().sort_values(ascending=False)

# %%
# higest run outs
ro = data.groupby('batsman')['dismissal_kind'].value_counts().unstack()
ro['run out'].sort_values(ascending=False).head()

# %%
# run rate of each batsman
rr = (data.groupby('batsman')['total_runs'].sum() /
      data.groupby('batsman')['ball'].size())
rr.sort_values(ascending=False).head()

# %%
# no of 4s
n4 = data.groupby('batsman')['batsman_runs'].value_counts().unstack()
n4[4].sort_values(ascending=False).head()

# %%
# no of 6s
n6 = data.groupby('batsman')['batsman_runs'].value_counts().unstack()
n6[6].sort_values(ascending=False).head()

# %%
# no of half centuries by each player(or top half century players)
hc = data.groupby(['match_id', 'batsman']).sum()
(hc[hc['batsman_runs'] > 50].groupby('batsman').size().
 sort_values(ascending=False))

# %%
# no of full-centuries
fc = data.groupby(['match_id', 'batsman']).sum()
(fc[fc['batsman_runs'] > 50].groupby('batsman').size().
 sort_values(ascending=False))

# %%
# no of balls faced by each batsaman
data.groupby('batsman')['ball'].size().sort_values(ascending=False)


# %%
# Bowler who gave more extra runs
(data.groupby('bowler')['extra_runs']
 .sum().sort_values(ascending=False).head(10))

# %%
# which team took highest wickets
w_t = data.groupby('bowling_team')['dismissal_kind'].value_counts().unstack()
w2_t = (w_t[['bowled', 'caught', 'lbw', 'caught and bowled', 'stumped',
        'hit wicket']])
w2_t['tot'] = total(w2_t)
w2_t['tot'].sort_values(ascending=False).head()

# %%
# no of balls with zero runs
data[data['total_runs'] == 0].shape[0]

# %%
# balls of each bowler with zero runs to the batsman
(data.groupby('bowler')['total_runs'].value_counts().unstack()[0]
 .sort_values(ascending=False).head())

# ---------------------------------------------------------------------
# %%
# No of balls by each bowler for which the batsman got 0 runs
(data[data['batsman_runs'] == 0].groupby('bowler').size()
 .sort_values(ascending=False).head(10))

# %%
# batsman dismissed in the first over itself
d_o1 = data[data['over'] == 1]
(d_o1[d_o1['batsman'] == d_o1['player_dismissed']].groupby('batsman')
 .size().sort_values(ascending=False).head(10))

# %%
# batsman dismissed in the first ball itself
d_o1 = data[data['ball'] == 1]
(d_o1[d_o1['batsman'] == d_o1['player_dismissed']].groupby('batsman')
 .size().sort_values(ascending=False).head(10))

# %%
# no of times batsman scores are less than 10
s10 = data.groupby(['match_id', 'batsman'])['batsman_runs'].sum()
s10[s10 < 10].groupby('batsman').size().sort_values(ascending=False).head()

# %%
# matches in which super overs occured
data[data['is_super_over'] == 1]['match_id'].unique()
# no of balls played in superover in each match
data[data['is_super_over'] == 1].groupby('match_id').size()
