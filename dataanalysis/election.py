"""US 2012 election dataset."""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
data = pd.read_csv('~/data/P00000001-ALL.csv')
data.head(20)
data.isnull().sum()
data['memo_cd'].unique()
data[data.columns].nunique()
data['cmte_id'].nunique()
data['contb_receipt_amt'].min()
data['contb_receipt_amt'].max()
data['contb_receipt_amt']
data['contb_receipt_dt'].min()
data['contb_receipt_dt'].max()

# %%
# contributions to candidate
high_contb = (data.groupby('cand_nm')['contb_receipt_amt'].sum()
              .sort_values(ascending=False))
plt.xticks(rotation=90)
x = sns.barplot(x=high_contb.index, y=high_contb.values)

# %%
# who contributed highest
h1 = (data.groupby('contbr_nm')['contb_receipt_amt'].sum()
      .sort_values(ascending=False)).head()
plt.xticks(rotation=90)
x = sns.barplot(x=h1.index, y=h1.values)

# %%
# percentage of refund contributions out of total contribution
l1 = len(data[data['contb_receipt_amt'] < 0])
(l1/len(data)) * 100


# %%
# Total Amount refunded
data[data['contb_receipt_amt'] < 0]['contb_receipt_amt'].sum()

# %%
# contributions to each committe
com_contb = data.groupby('cmte_id')['contb_receipt_amt'].sum()
plt.xticks(rotation=90)
x = sns.barplot(x=com_contb.index, y=com_contb.values)

# %%
# which state gave the most no of contributions
data.groupby(['contbr_st', 'contbr_nm']).size().sort_values(ascending=False)

# ----------------------------------------------------------------------
# %%
# total nof candidates participated
data['cand_nm'].nunique()

# %%
# contributions from each state
(data.groupby('contbr_st')['contb_receipt_amt'].sum().
 sort_values(ascending=False))

# %%
# Which city in CA gave the highest contributions
(data[data['contbr_st'] == 'CA'].groupby('contbr_city')['contb_receipt_amt'].
 sum().sort_values(ascending=False))

# %%
# no of contributors to each committe
data.groupby('cmte_id').nunique()['contbr_nm'].sort_values(ascending=False)

# %%
# from which place barack obama has got the highest contributions
(data[data['cand_nm'] == 'Obama, Barack'].groupby(['contbr_st', 'contbr_city'])
 ['contb_receipt_amt'].sum().sort_values(ascending=False).head())

# %%
# no of contributors to each candidate
data.groupby('cand_nm').nunique()['contbr_nm'].sort_values(ascending=False)

# %%
# most common contribution
data['contb_receipt_amt'].value_counts().head()

# %%
# average donation
don = data[data['contb_receipt_amt'] > 0]
don['contb_receipt_amt'].mean()

# %%
# to which occupation most people belong to of those who contributed to obama
(data[data['cand_nm'] == 'Obama, Barack'].groupby('contbr_occupation')
 ['contb_receipt_amt'].sum().sort_values(ascending=False).head())

# %%
# most of the homemakers contributed to which candidate
(data[data['contbr_occupation'] == 'HOMEMAKER'].groupby('cand_nm').nunique()
 ['contbr_nm'].sort_values(ascending=False))

# -----------------------------------------------------------------------
# %%
# contributions to barack obama in different states.
(data[data['cand_nm'] == 'Obama, Barack'].groupby('contbr_st')
 ['contb_receipt_amt'].sum().sort_values(ascending=False).head(10))

# %%
# places where romney miott got more contributions than Obama
c1 = (data[data['cand_nm'] == 'Obama, Barack'].groupby('contbr_st')
      ['contb_receipt_amt'].sum())
c2 = (data[data['cand_nm'] == 'Romney, Mitt'].groupby('contbr_st')
      ['contb_receipt_amt'].sum())
df_c2 = pd.DataFrame(c2)
df_c1 = pd.DataFrame(c1)
con = pd.merge(df_c1, df_c2, left_index=True, right_index=True)
con[con['contb_receipt_amt_y'] > con['contb_receipt_amt_x']]

# %%
# minimum contribution to each candidate
(data[data['contb_receipt_amt'] > 0].groupby('cand_nm')['contb_receipt_amt']
 .min())

# %%
# average contribution to each candidate
(data[data['contb_receipt_amt'] > 0].groupby('cand_nm')['contb_receipt_amt']
 .mean().sort_values(ascending=False))

# %%
# candidate with highest refund
(data[data['contb_receipt_amt'] < 0].groupby('cand_nm')['contb_receipt_amt']
 .sum().sort_values())
