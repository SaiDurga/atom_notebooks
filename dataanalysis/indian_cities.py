"""Data analysis on indian cities dataset."""

import pandas as pd
import matplotlib.pyplot as plt

# %%
# load the dataset
data = pd.read_csv('~/data/cities_r2.csv')
data.head()

# %%
data.shape
data.info()

# %%
# Check for nans
data.isnull().any()
data.isnull().sum()
# There are no nans

# %%
# no of states in india
data['state_name'].nunique()
# there are 29 states in india

# %%
# no of cities in india
data['name_of_city'].nunique()
# there are 492 cities in india

# %%
# state with more no of cities
m_c = (data.groupby('state_code')['name_of_city'].nunique()
       .sort_values(ascending=False))
code = m_c.head(1).index[0]
data[data['state_code'] == code]['state_name'].values[0]

# %%
# no of cities in each state
cities = data.groupby('state_name')['name_of_city'].nunique()
plt.figure()
plt.title("no of cities in each state")
cities.plot(kind='bar')

# %%
# plot a pie chart with percentages of cities in each state
cities = data.groupby('state_name')['name_of_city'].nunique()
tot_cities = data['name_of_city'].nunique()
cities1 = cities/tot_cities * 100
plt.figure(figsize=(15, 10))
cities1.plot(kind='Pie', rotatelabels=True, fontsize=9)

# %%
# highest populated city and the state in which it is located
h_p = data['population_total'].sort_values(ascending=False).head()
h_p1 = h_p.index[0]
data.loc[h_p1][['name_of_city', 'state_name']]

# %%
# highly populated state
h_s = data.groupby('state_name')['population_total'].sum()
h_s.sort_values(ascending=False).head()

# %%
# population distribution of each state
h_s = data.groupby('state_name')['population_total'].sum()
plt.title('population in each state')
h_s.plot(kind='bar')

# %%
# no of cities with more female population than male
data[data['population_female'] > data['population_male']].shape
# 60 cities
data[data['population_female'] > data['population_male']]['name_of_city']
(data[data['population_female'] > data['population_male']]['state_name']
 .unique())

# %%
# cities/states with more kids
data.loc[data['0-6_population_total'] == data['0-6_population_total'].max()]
# delhi

# %%
# highest female literates
f_l = data['literates_female'].sort_values(ascending=False).head(1).index[0]
data.loc[f_l][['name_of_city', 'state_name']]
# highest female effective literacy rate
f_l1 = (data['effective_literacy_rate_female'].sort_values(ascending=False)
        .head(1).index[0])
data.loc[f_l1][['name_of_city', 'state_name']]
# more no female literates in mumbai but effective literacy rate is high in
# aizwal of mizora,

# %%
# city /state with high literacy
hl = data['effective_literacy_rate_total'].max()
(data[data['effective_literacy_rate_total'] == hl]
 [['name_of_city', 'state_name']])

# %%
# total population of the country living in cities
data['population_total'].sum()

# %%
# overall literacy rate of the country in cities
pop = data['population_total'].sum()
lit = data['literates_total'].sum()
lit/pop * 100
# Only 77.2% of the people living in cities across the country are literates

# %%
# states with only one city
c1 = data.groupby('state_name')['name_of_city'].nunique()
c2 = pd.DataFrame(c1)
c2[c2['name_of_city'] == 1].index

# %%
# cities with not even 50% literacy rate
(data[data['effective_literacy_rate_total'] < 50]
 [['name_of_city', 'state_name']])
# Sambhal in the state UP is the only city with not even 50% of literacy rate

# %%
# state with high female literacy than male
(data[data['effective_literacy_rate_female']
 > data['effective_literacy_rate_male']])
# no

# %%
# add a column that indicates the % of kids in total population in each city
data1 = data
kids = data1['0-6_population_total']/data['population_total']
kids
data1['%_of_kids'] = kids * 100
data1.head()

# %%
# city/state with highest percentage of kids
hk = data1['%_of_kids'].max()
data1[data1['%_of_kids'] == hk][['name_of_city', 'state_name']]

# %%
# percentage of graduates in each city out of total population
gra = data['total_graduates']/data['population_total'] * 100
gra1 = gra.sort_values(ascending=False).head().index[0]
data.loc[gra1][['name_of_city', 'state_name']]

# %%
# high literacy rate state
hls = data.groupby('state_name').sum()
hls1 = hls['literates_total']/hls['population_total'] * 100
hls1.sort_values(ascending=False).head()

# %%
# city with highest literacy rate in AP
d_ap = data[data['state_name'] == 'ANDHRA PRADESH']
d_ap1 = d_ap['effective_literacy_rate_total'].sort_values(ascending=False)
d_ap2 = d_ap1.head(1).index[0]
data.loc[d_ap2]['name_of_city']

# %%
# cities from each state which are in top 500
top = data.sort_values(by='literates_total').head(500)
top1 = top.groupby('state_name')['name_of_city'].size()
plt.figure()
plt.title("no of cities from each city in top 500")
top1.plot(kind='bar')


# %%
# average sexratio of states in india
data.groupby('state_name')['sex_ratio'].mean()


# top developing states (Top States having better Total Literacy Rates,
# Sex-Ratio and Graduation Ratio)
sta = data.groupby('state_name').sum()
sta_lit = sta['literates_total'].sort_values(ascending=False).head(10)
sta_s_r = sta['sex_ratio'].sort_values(ascending=False).head(10)
sta_gra = sta['total_graduates'].sort_values(ascending=False).head(10)
sta_lit1 = pd.DataFrame(sta_lit)
sta_s_r1 = pd.DataFrame(sta_s_r)
sta_gra1 = pd.DataFrame(sta_gra)
m1 = pd.merge(sta_lit1, sta_s_r1, left_index=True, right_index=True)
m2 = pd.merge(m1, sta_gra1, left_index=True, right_index=True)
m2

# %%
# How many literates are graduate within each states?
gra_lat = data.groupby('state_name').sum()
gra_per = gra_lat['total_graduates']/gra_lat['literates_total'] * 100
gra_per.plot(kind='bar')
