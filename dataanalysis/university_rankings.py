"""Data analysis on world university rankings dataset."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

# %%
# loading the dataset
data = pd.read_csv('~/data/cwurData.csv')
data.head()

# %%
data.shape
data.info()

# %%
# Check for nans
data.isnull().any()
data.isnull().sum()

# %%
# no of universities considered for ranking
data['institution'].nunique()

# %%
# universitirs of  which countries
data['country'].nunique()
# universities from over 59 countries are considered

# %%
# which country universities are in top 10
data[data['world_rank'] <= 10]['country'].value_counts()
# universities from USA and United Kingdom

# %%
# Top universities in each country
t_u = data[data['national_rank'] == 1]
t_u.groupby('country')['institution'].unique()

# %%
# no of universities given ranks from each country
u_c = data.groupby('country')['institution'].nunique()
plt.figure(figsize=(15, 10))
plt.title("no of universities from each country")
plt.ylabel("no of institutions")
plt.xticks(fontsize=12, rotation=90)
u_c.plot(kind='bar')

# %%
# no of univesities that were given below 100 rank in each country
r_100 = data[data['world_rank'] <= 100]
r_100_1 = (r_100.groupby('country')['institution'].nunique()
           .sort_values(ascending=False))
plt.figure(figsize=(10, 5))
plt.title("no of universities from each country with below 100 rank")
plt.ylabel("no of institutions")
plt.xticks(fontsize=12, rotation=90)
r_100_1.plot(kind='bar')

# %%
# In which years rankings were given
data['year'].unique()

# %%
# Top 10 universities in every year
t_10 = data[data['world_rank'] <= 10]
t_10.groupby('year')['institution'].unique()

# %%
# Univesity with top 1 position in every year
data[data['world_rank'] == 1].groupby('year')['institution'].value_counts()

# %%
# what percent of institutions are from USA of those below 100 every year
r_100 = data[data['world_rank'] <= 100]
r_100_u = r_100[r_100['country'] == 'USA']
y1 = r_100.groupby('year').size()
y2 = r_100_u.groupby('year').size()
per = y2/y1 * 100
plt.title("percentage of institutions from USA  below 100 every year")
plt.ylabel('no of institutions')
per.plot(kind='bar')

# %%
# pie chart  with percentages of countries in below 100 in year 2015
p1 = data[np.all([data['year'] == 2015, data['world_rank'] <= 100], axis=0)]
p2 = p1.groupby('country').size()
p3 = p2/100 * 100
plt.figure(figsize=(10, 20))
plt.title("percentages of countries in below 100 in year 2015")
plt.ylabel('percentages')
p3.plot(kind='Pie', rotatelabels=True, fontsize=10)

# %%
# Realtion b/w score and world ranks
x = sns.lmplot(data=data, x='score', y='world_rank', hue='year', fit_reg=False)

# %%
# relation b/w quality education and alumni employment
data1 = data[data['year'] == 2015]
x = (sns.lmplot(data=data1, x='quality_of_education', y='alumni_employment',
     fit_reg=True))

# %%
# Universities that are best in providing quality education
qe = data.groupby('year')
(qe['quality_of_education']
 .apply(lambda x: x.sort_values(ascending=False).head()))

# %%
# universities that have higher alumni employement
ae = data.groupby('year')
(ae['alumni_employment']
 .apply(lambda x: x.sort_values(ascending=False).head()))

# %%
#  relation b/w quality of faculty and publications
(sns.lmplot(data=data, x='quality_of_faculty', y='publications', hue='year',
 fit_reg=True))
# in 2012 and 2013 its constant but in 2014 and 2015 publications increased
# with increase in quality of faculty

# %%
# sort countries based on their average alumni employement rank
al_em = data.groupby('country')['alumni_employment'].mean()
al_em.sort_values().head(10)
plt.figure()
plt.title("average alumni employement in each country")
plt.ylabel("average alumni employement")
plt.xticks(fontsize=8)
al_em.plot(kind='bar')


# %%
# average quality of education rank in each institution through different years
q_e = data.groupby('institution')['quality_of_education'].mean()
q_e1 = q_e.sort_values().head(30)
plt.figure()
plt.title("average quality of education in each institution")
plt.ylabel("average quality of education")
plt.xticks(fontsize=8)
q_e1.plot(kind='bar')
q_e1

# %%
# Ranks of few universities in all years(plot)
institutions = (['Harvard University', 'Massachusetts Institute of Technology',
                'Stanford University', 'University of Cambridge'])
data2 = data.loc[data['institution'].isin(institutions)]
plt.title("ranks of different institutions in 4 years")
plt.xticks(rotation=90)
x = sns.barplot(data=data2, x='institution', y='world_rank', hue='year')

# %%
# heat map for input variables and output
d3 = (data[['quality_of_education', 'alumni_employment', 'quality_of_faculty',
      'publications', 'influence', 'citations', 'patents', 'world_rank']])
d4 = d3.corr()
d4
sns.heatmap(d4)

# %%
# no of students leaving out in particular year in different countries
d_2015 = data[data['year'] == 2015]
s_c = d_2015.groupby('country')['citations'].sum()
plt.figure()
plt.title("no of students leaving out in 2015")
plt.ylabel("country")
plt.xticks(fontsize=8)
s_c.plot(kind='bar')

# %%
# no of institutions from India
d_in = data[data['country'] == 'India']
d_in['institution'].unique()

# %%
# variation in ranking for the institutions in india
vr = d_in.groupby(['institution', 'year'])['world_rank']
vr1 = vr.apply(lambda x: x.values)
vr2 = pd.DataFrame(vr1)
vr3 = vr2.reset_index(level=(0, 1))
plt.figure(figsize=(15, 10))
plt.xticks(rotation=90)
x = sns.barplot(data=vr3, x='institution', y='world_rank', hue='year')

# %%
d_in = data[data['country'] == 'India']
vin = d_in.groupby(['institution', 'year'])['national_rank']
vin1 = vin.apply(lambda x: x.values)
vin2 = pd.DataFrame(vin1)
vin3 = vin2.reset_index(level=(0, 1))
plt.figure(figsize=(15, 10))
plt.xticks(rotation=90)
x = sns.barplot(data=vin3, x='institution', y='national_rank', hue='year')
