"""IPL data analysis."""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
data1 = pd.read_csv('~/data/matches.csv')
data1.head()
data1.shape
data2 = pd.read_csv('~/data/deliveries.csv')
data2.head()
data2.shape
data1['id'].unique()
data2['match_id'].unique()
data = pd.merge(data1, data2, left_on='id', right_on='match_id')
data.head()
data.shape
data.isnull().sum()

# %%
# drop umpire 3 column
data = data.drop(['umpire3'], axis='columns')
data.shape

# -----------------------------------------------------------------------
# %%
# bowler who took highest wickets in each season
w = (data.groupby(['season', 'bowler'])['dismissal_kind']
     .value_counts().unstack())
w1 = (w[['bowled', 'caught', 'lbw', 'caught and bowled', 'stumped',
      'hit wicket']])


def total(data):
    """Add elements in a row."""
    res = []
    for i in range(len(data)):
        res.append(data.iloc[i].sum())
    return res


w1['total'] = total(w1)
w2 = w1.reset_index()
w3 = w2.set_index('bowler')
w3.groupby('season')['total'].apply(lambda x: x.nlargest(1))


# %%
# batsman with highest runs in each season
hr = data.groupby(['season', 'batsman'])
hr1 = hr.apply(lambda x: x['total_runs'].sum())
hr2 = pd.DataFrame(hr1, columns=['total_runs'])
hr3 = hr2.reset_index(level=0)
hr3.groupby('season')['total_runs'].apply(lambda x: x.nlargest(1))

# %%
# team analysis
m_p = data.groupby('batting_team')['match_id'].nunique()
m_w = data.groupby('winner')['match_id'].nunique()
m_p1 = pd.DataFrame(m_p)
m_p1.columns = ['matches_played']
m_w1 = pd.DataFrame(m_w)
m_w1.columns = ['matches_won']
t_a = pd.merge(m_p1, m_w1, left_index=True, right_index=True).reset_index()
t_a1 = (pd.melt(t_a, id_vars=['batting_team'],
        value_vars=['matches_played', 'matches_won'],
        value_name='no of matches played and won'))
t_a1
plt.xticks(rotation=90)
x = (sns.barplot(data=t_a1, x='batting_team', y='no of matches played and won',
     hue='variable'))

# %%
# toss decisions in each season
t_d = data.groupby('season')['toss_decision'].value_counts()
t_d1 = pd.DataFrame(t_d, columns=['toss_decision'])
t_d1.columns = ['values']
t_d2 = t_d1.reset_index(level=[0, 1])
t_d2
x = sns.barplot(x='season', y='values', hue='toss_decision', data=t_d2)

# %%
# runrate in each season for srh
srh = data[data['batting_team'] == 'Sunrisers Hyderabad']
rr = srh.groupby('season')['total_runs'].sum()
rr1 = srh.groupby('season')['ball'].size()
rr2 = rr/rr1
plt.xticks(np.arange(2013, 2018, step=1))
plt.plot(rr2)


# %%
# no of 4s and 6s in each season
d_4 = data[data['batsman_runs'] == 4]
d_4_s = d_4.groupby('season').size()
d4s = pd.DataFrame(d_4_s, columns=['no of 4s'])
d4s1 = d4s.reset_index()
d4s1
x = sns.barplot(data=d4s1, x='season', y='no of 4s')

# %%
d_6 = data[data['batsman_runs'] == 6]
d_6_s = d_6.groupby('season').size()
d6s = pd.DataFrame(d_6_s, columns=['no of 6s'])
d6s1 = d6s.reset_index()
d6s1
x = sns.barplot(data=d6s1, x='season', y='no of 6s')


# %%
# Winners by Year
w_y = data[['season', 'id', 'winner']]
w_y1 = w_y.drop_duplicates()
w_y2 = w_y1.set_index('winner')
w_y2.groupby('season')['id'].apply(lambda x: x.nlargest(1))

# %%
# player with highest [score(in a single match)
(data.groupby(['id', 'batsman'])['total_runs'].sum()
 .sort_values(ascending=False).head())

# %%
# team with highest no of 4's in each season
d_4 = data[data['batsman_runs'] == 4]
d_4_1 = d_4.groupby(['season', 'batting_team'])
d_4_2 = d_4_1.apply(lambda x: x['batsman_runs'].size)
d_4_3 = pd.DataFrame(d_4_2, columns=['no of 4s'])
d_4_4 = d_4_3.reset_index(level=[0, 1])
d_4_4
d_4_4.groupby('season')['no of 4s'].apply(lambda x: x.nlargest(1))

# %%
# batsman with highest runrate in  each season
hr = data.groupby(['season', 'batsman'])['total_runs'].sum()
hr1 = data.groupby(['season', 'batsman'])['ball'].size()
hr2 = hr/hr1
hr3 = pd.DataFrame(hr2, columns=['runrate']).reset_index(level=0)
hr3.groupby('season')['runrate'].apply(lambda x: x.nlargest(1))

# %%
# variation in runs by virat kohli over different seasons
v_kohli = data[data['batsman'] == 'V Kohli']
v_h = v_kohli.groupby('season')['total_runs'].sum()
plt.figure(figsize=(10, 5))
plt.xticks(np.arange(2008, 2018, step=1))
plt.plot(v_h)
plt.title("runs by virat kohli in each season")
plt.xlabel("seasons")
plt.ylabel("runs")

# %%
# Percentage of boundary runs out of total runs for each batsman
d1 = data[data['batsman_runs'] == 4].groupby('batsman')['batsman_runs'].sum()
d2 = data.groupby('batsman')['batsman_runs'].sum()
(d1/d2 * 100).head(10)

# %%
# dismissal kind for the top batsman
t_b = data.groupby('batsman')['total_runs'].sum().nlargest(1)
bm = t_b.index[0]
d_k = data[data['batsman'] == bm]['dismissal_kind'].value_counts()
d_k1 = pd.DataFrame(d_k).reset_index()
d_k1.columns = ['kind', 'no of times']
d_k1
plt.xticks(rotation=90)
x = sns.barplot(data=d_k1, x='kind', y='no of times')

# %%
# players with zero total runs in each season
z_r = data.groupby(['season', 'batsman'])
z_r1 = z_r.apply(lambda x: x['total_runs'].sum())
z_r2 = pd.DataFrame(z_r1, columns=['total_runs'])
z_r2[z_r2['total_runs'] == 0]

# -----------------------------------------------------------------
# %%
# no of runs made by winning team in each season
w_y3 = w_y2.groupby('season')['id'].apply(lambda x: x.nlargest(1))
w_y4 = pd.DataFrame(w_y3).reset_index(level=1)
w_y4
for i, j in zip(w_y4['winner'], w_y4.index):
    d1 = data[data['season'] == j]
    d2 = data[data['winner'] == i]
    tr = d2['total_runs'].sum()
    print(j, i, tr)

# %%
w = (data.groupby(['season', 'bowler'])['dismissal_kind']
     .value_counts().unstack())
w1 = (w[['bowled', 'caught', 'lbw', 'caught and bowled', 'stumped',
      'hit wicket']])

# %%
# no of wickets taken by winning team in each season
w_y4
for i, j in zip(w_y4['winner'], w_y4.index):
    d1 = data[data['season'] == j]
    d2 = data[data['winner'] == i]
    d3 = d2.groupby('dismissal_kind').size()
    d4 = (pd.DataFrame(d3).loc[['bowled', 'caught', 'lbw', 'caught and bowled',
          'stumped', 'hit wicket']].sum())
    print(i, j, d4.values)

# %%
# who got highest player of match awards in each season
p_m = data[['season', 'id', 'player_of_match']]
p_m1 = (p_m.drop_duplicates().groupby('season')['player_of_match']
        .value_counts())
p_m1
p_m2 = pd.DataFrame(p_m1).reset_index(level=0)
p_m2
p_m3 = p_m2.groupby('season')['player_of_match'].apply(lambda x: x.nlargest(1))
p_m3

# %%
# no of runs made by the one who got highest man of match awards in each season
p_m4 = pd.DataFrame(p_m3)
p_m4.columns = ['no of times']
p_m5 = p_m4.reset_index(level=[1])
p_m5.index
for i, j in zip(p_m5['player_of_match'], p_m5.index):
    d1 = data[data['season'] == j]
    d2 = d1[d1['batsman'] == i]
    runs = d2['total_runs'].sum()
    print(j, i, runs)

# %%
# no of runs by each team in each seasons
r_t = data.groupby(['season', 'batting_team'])['total_runs'].sum()
r_t1 = pd.DataFrame(r_t).reset_index(level=[0, 1])
plt.figure(figsize=(15, 5))
r_t1
x = sns.pointplot(data=r_t1, x='season', y='total_runs', hue='batting_team')

# %%
# no of wickets taken by each team in each season
d1 = (data.groupby(['season', 'bowling_team'])['dismissal_kind']
      .value_counts().unstack())
d2 = (d1[['bowled', 'caught', 'lbw', 'caught and bowled', 'stumped',
      'hit wicket']])

d2['total'] = total(d2)
d3 = d2.reset_index()
d4 = d3.set_index('bowling_team')
d5 = d4.groupby('season')['total'].apply(lambda x: x.nlargest(1))
d5

# %%
# no of wickets taken by a particular bowler in each season
# 'TS Mills'
m = data[data['bowler'] == 'SL Malinga']
m1 = m.groupby(['season', 'dismissal_kind']).size().unstack()
m2 = m1[['bowled', 'caught', 'lbw', 'caught and bowled']]
m2['tot'] = total(m2)
m3 = m2.reset_index()
plt.title("no of wickets by sl malinga in each season")
x = sns.barplot(data=m3, x='season', y='tot')

# no of wickets taken by the one who took highest wickets in each season

# %%
# most dismissal kind in each season
dk = data.groupby('season')['dismissal_kind'].value_counts()
dk1 = pd.DataFrame(dk)
dk1.columns = ['no of times']
dk2 = dk1.reset_index(level=[0])
dk2.groupby('season')['no of times'].apply(lambda x: x.nlargest(1))

# %%
# batsman with highest half centuries in each season
h_c = data.groupby(['season', 'batsman', 'id'])['total_runs'].sum()
h_c1 = pd.DataFrame(h_c)
h_c1
h_c2 = h_c1[h_c1['total_runs'] >= 50]
h_c3 = h_c2[h_c2['total_runs'] < 100]
h_c3['no of half cent'] = h_c3['total_runs']/50
h_c4 = h_c3.reset_index(level=0)
h_c4.groupby('season')['no of half cent'].apply(lambda x: x.nlargest(1))

# %%
# batsman with highest centuries in each season
c = data.groupby(['season', 'batsman', 'id'])['total_runs'].sum()
c1 = pd.DataFrame(c)
c1
c2 = c1.loc[c1['total_runs'] >= 100]
c2['no of centuries'] = c1['total_runs']/100
c2
c3 = c2.reset_index(level=0)
c3.groupby('season')['no of centuries'].apply(lambda x: x.nlargest(1))

# %%
# top catchers
data[data['dismissal_kind'] == 'caught']['fielder'].value_counts().head()

# %%
# top stumpers
data[data['dismissal_kind'] == 'stumped']['fielder'].value_counts().head()

# %%
# who did highest no of runs in winning team in each season
for i, j in zip(w_y4['winner'], w_y4.index):
    d1 = data[data['season'] == j]
    d2 = d1[d1['batting_team'] == i]
    d3 = d2.groupby('batsman')['total_runs'].sum().sort_values(ascending=False)
    print(j, i, d3.head(1))

# %%
# who took highest no of wickets in winning team of each season
for i, j in zip(w_y4['winner'], w_y4.index):
    d1 = data[data['season'] == j]
    d2 = d1[d1['bowling_team'] == i]
    d3 = d2.groupby('bowler')['dismissal_kind'].value_counts().unstack()
    d3['tot'] = total(d3)
    d4 = d3.reset_index()
    d5 = d4[d4['tot'] == d4['tot'].max()]
    print(i, j, d5.bowler.values[0])

# ------------------------------------------------------------------------
# %%
# differnt runs out of total runs for a player
vk_runs = data[data['batsman'] == 'V Kohli']
vk_runs1 = (vk_runs[['wide_runs', 'bye_runs', 'legbye_runs', 'noball_runs',
            'penalty_runs', 'batsman_runs', 'extra_runs', 'total_runs']])
vk_runs2 = vk_runs1.sum()
plt.figure(figsize=(10, 20))
plt.title("different runs out of total runs made by kohli")
plt.ylabel('runs')
vk_runs2.plot(kind='bar', fontsize=20)

# %%
# different wickets by SL Malinga
dj = data[data['bowler'] == 'SL Malinga']
dj['player_dismissed'].unique()
dj1 = dj[dj['player_dismissed'] != 'nan']
dj2 = dj1[dj1['dismissal_kind'] != 'nan']
dj3 = dj2.groupby('id')['dismissal_kind'].value_counts()
dj4 = pd.DataFrame(dj3)
dj4.columns = ['no of times']
dj5 = dj4.reset_index(level=1)
dj6 = dj5[dj5['dismissal_kind'] != 'run out']
dj7 = dj6.groupby('dismissal_kind').sum()
dj7.plot(kind='Pie', subplots=True)

# %%
# runs of any two batsman one vs other
# ms dhoni and v kohli
msd = data[data['batsman'] == 'MS Dhoni']
vkl = data[data['batsman'] == 'V Kohli']
msd1 = pd.DataFrame(msd.groupby('season')['total_runs'].sum())
vkl1 = pd.DataFrame(vkl.groupby('season')['total_runs'].sum())
ovo = pd.merge(msd1, vkl1, left_index=True, right_index=True).reset_index()
ovo.columns = ['season', 'dhoni', 'kohli']
ovo1 = pd.melt(ovo, id_vars=['season'],  var_name='batsman', value_name='runs')
x = sns.pointplot(data=ovo1, x='season', y='runs', hue='batsman')

# %%
# % of runs by kohli in rcb in every season
rcb = data[data['batting_team'] == 'Royal Challengers Bangalore']
rcb1 = rcb[rcb['batsman'] == 'V Kohli']
k1 = rcb.groupby('season')['total_runs'].sum()
k2 = rcb1.groupby('season')['total_runs'].sum()
k3 = k2/k1 * 100
plt.xlabel("season")
plt.ylabel("Percentage")
plt.title("percentage of runs of kohli in rcb in each season")
plt.plot(k3)

# %%
# with which non striker kohli scored more runs
ns = data[data['batsman'] == 'V Kohli']
ns1 = ns.groupby('non_striker')['total_runs'].sum().nlargest(1)
ns1
# CH Gayle

# %%
# under whose bowling kohli scored more runs
b = data[data['batsman'] == 'V Kohli']
b1 = b.groupby('bowler')['total_runs'].sum().nlargest(1)
b1
# A Mishra

# %%
# no of matches won by srh when batting first or fielding first


def first_played(df):
    """Function."""
    if df['toss_decision'] == 'bat':
        return df['toss_winner']
    else:
        if df['toss_winner'] != df['team1']:
            return df['team1']
        else:
            return df['team2']


srh = data[data['winner'] == 'Sunrisers Hyderabad']
srh['bat_first_team'] = srh.apply(first_played, axis=1)
srh1 = srh[srh['bat_first_team'] == 'Sunrisers Hyderabad']
srh1['id'].nunique()
srh2 = srh[srh['bat_first_team'] != 'Sunrisers Hyderabad']
srh2['id'].nunique()

# %%
# average runs by srh when batting first
data1 = data
data1['bat_first_team'] = data1.apply(first_played, axis=1)
avg_r = data1[data1['bat_first_team'] == 'Sunrisers Hyderabad']
avg_r['total_runs'].sum()/avg_r['id'].nunique()

# %%
# % of wideballls by a particular bowler
hs = data[data['bowler'] == 'Harbhajan Singh']
hs1 = hs[hs['wide_runs'] >= 1]
len(hs1)/len(hs) * 100

# %%
# which team did highest runs in each season
tr = data.groupby(['season', 'batting_team'])['total_runs'].sum()
tr1 = pd.DataFrame(tr).reset_index(level=0)
tr1.groupby('season')['total_runs'].apply(lambda x: x.nlargest(1))

# %%
# no of matches in each season
data.groupby('season')['id'].nunique()

# %%
# no of matches played by each team in each season
n_m = data.groupby(['season', 'batting_team'])['id'].nunique()
n_m1 = pd.DataFrame(n_m)
n_m1.columns = ['season', 'team', 'no of matches']
plt.figure(figsize=(15, 5))
x = sns.pointplot(data=n_m1, x='season', y='no of matches', hue='team')

# %%
# batsman not out


def not_out(match):
    """Not out batsman in each match."""
    m1 = data[data['id'] == match]
    bm = m1['batsman'].unique()
    pd = m1['player_dismissed'].unique()
    res = []
    for i in bm:
        if i not in pd:
            res.append(i)
    return (match, res)


matches = data['id'].unique()
for i in matches:
    print(not_out(i))
