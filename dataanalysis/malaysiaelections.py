"""Data analysis on malaysia election results- 2018."""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
# loading the dataset
data = pd.read_csv('~/data/malasian_eclection.csv')
data.head()

# %%
data.shape
data.info()
data.describe()

# %%
# check for nans
data.isnull().any()
# no nulls

# %%
data['Gender'].unique()
# Gender L = Male(Lelaki), P=Female(Perempuan)

# %%
# replace values in gender column
data['Gender'].unique()
data['Gender'] = data['Gender'].replace({'L': 'Male', 'P': 'Female'})
data.head()
data['Gender'].unique()

# %%
# Total no of parties participated in elections
data['Candidate Name'].nunique()
# No of candidates from each party
data.groupby('Candidate Party').size()

# %%
# top parties through which most of the candidates participated
data.groupby('Candidate Party').size().sort_values(ascending=False).head()

# %%
# parties with 100% winning rate
c_won_par = data[data['Candidate Win'] == 1].groupby('Candidate Party').size()
cwp = pd.DataFrame(c_won_par)
cwp
c_part = data.groupby('Candidate Party').size()
cp = pd.DataFrame(c_part)
cp
win_per = pd.merge(cwp, cp, left_index=True, right_index=True)
win_per[win_per['0_x'] == win_per['0_y']].index[0]

# %%
# % of winning in each party
win_per1 = win_per['0_x']/win_per['0_y'] * 100
win_per1

# %%
# proportion of males and females participated in election
m = data[data['Gender'] == 'Male']
f = data[data['Gender'] == 'Female']
m_r = m.shape[0]/data.shape[0]
m_r
f_r = len(f)/len(data)
f_r

# %%
# Party with highest proportion of female candidates
f.groupby('Candidate Party').size().sort_values(ascending=False)

# %%
# party which gave more importance to female(more frmale candidates than male)
m1 = m.groupby('Candidate Party').size()
f1 = f.groupby('Candidate Party').size()
m1
f1
m2 = pd.DataFrame(m1)
f2 = pd.DataFrame(f1)
m_i = pd.merge(m2, f2, left_index=True, right_index=True)
m_i[m_i['0_x'] < m_i['0_y']]
# no

# %%
# candidates of which pekerjaan won highest
c_won = data[data['Candidate Win'] == 1]
c_won.groupby('Pekerjaan').size().sort_values(ascending=False).head()

# %%
# no of seats won by each party for each gender
c1 = c_won.groupby(['Gender', 'Candidate Party']).size()
c2 = pd.DataFrame(c1, columns=['seats_won'])
c2
c3 = c2.reset_index(level=(0, 1))
c3
plt.xticks(rotation=90)
x = sns.barplot(data=c3, x='Candidate Party', y='seats_won', hue='Gender')

# %%
# convert the column % of total Votes from string to float
s1 = data['% of total Votes']
s2 = []
for i in s1:
    s2.append(pd.to_numeric(i[:-1]))
print(s2)
s3 = pd.Series(s2)
data1 = data
data1['votes_float'] = s3
data1.head()

# %%
# candidate who won 50% of votesCandidate win
data1[data1['votes_float'] > 50].shape[0]
# 132 candidates won more than 50% of votes of all the total votes casted

# %%
# candidates with not even 10% of votes
data1[data1['votes_float'] < 5].shape[0]
# 74 candidates didn't secure even 5% votes

# %%
# candidate who secured more than 90% of the votes
data1[data1['votes_float'] > 90]['Candidate Name']

# %%
# convert the column % of total Votes from string to float
v1 = data['Total Votes Cast']
v2 = []
for i in v1:
    v2.append(float(i.replace(',', '')))
print(v2)
v3 = pd.Series(v2)
v3

data1['totalvotes_float'] = v3
data1.head()

# %%
# places with less than 15000 voters
data1[data1['totalvotes_float'] < 15000]['Seat Name'].nunique()

# %%
# place with maximum and minimum no of votes casted
minimum = data1['totalvotes_float'].min()
data1[data1['totalvotes_float'] == minimum]['Seat Name'].unique()[0]
maximum = data1['totalvotes_float'].max()
data1[data1['totalvotes_float'] == maximum]['Seat Name'].unique()[0]

# %%
# percentage of winnings of male and female who participated
m_win = m[m['Candidate Win'] == 1]
len(m_win)/len(m) * 100
f_win = f[f['Candidate Win'] == 1]
len(f_win)/len(f) * 100

# %%
# percentage of male and female in those who won
c1 = c_won.groupby('Gender').size()
c1[0]/c1.sum() * 100
c1[1]/c1.sum() * 100

# %%
# Candidates who didnt get 35% of votes but still won
c_won1 = data1[data1['Candidate Win'] == 1]
c_won1[c_won1['votes_float'] < 35]

# %%
# No of votes for each party(plot)
v_p = data1.groupby('Candidate Party')['totalvotes_float'].sum()
v_p.plot(kind='bar')

# %%
# Candidates who participated from more than one place
o_p = data.groupby('Candidate Name').size()
o_p1 = pd.DataFrame(o_p, columns=['places'])
o_p1[o_p1['places'] > 1]
# no

# %%
# place where more no of candidates compete
data.groupby('Seat Name').size().sort_values(ascending=False).head(10)

# %%
# places where there is no competition
n_c = data.groupby('Seat Name').size()
n_c1 = pd.DataFrame(n_c, columns=['members'])
n_c1[n_c1['members'] == 1]
# no
