"""Data analysis on chocolate bars dataset."""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# loading the dataset
data = pd.read_csv('~/data/flavors_of_cacao.csv')
data.head()

# %%
data.shape
data.info()

# %%
# Check for nans
data.isnull().any()
data.isnull().sum()

# ----------------------------------------------------------------------------
# DATA ANALYSIS

# %%
# rename the  columns
data.columns
data.columns = (['Company', 'BarName', 'REF', 'ReviewDate', 'CocoaPercent',
                'CompanyLocation', 'Rating', 'BeanType', 'BeanOrigin'])
data.head()

# %%
# no of different types of bars considered
data['BarName'].nunique()
# 1039 different kinds of chocolate bars are considered

# %%
# no of companies manufacturing choclate bars
data['Company'].nunique()
# 416 different companies produce chocolate bars

# %%
# Replace the country spelling Niacragua with Nicaragua
data['CompanyLocation'].nunique()
data1 = data.replace('Niacragua', 'Nicaragua')
data1['CompanyLocation'].nunique()

# %%
# company with more no of branches
c_c = data1.groupby('Company')
c_c1 = c_c.apply(lambda x: x['CompanyLocation'].nunique())
c_c2 = pd.DataFrame(c_c1, columns=['number'])
c_c2[c_c2['number'] > 1]
# Only one company 'spencer' with two branches in australia and USA


# %%
# distribution of companies across different countries
d_c = data1.groupby('CompanyLocation')['Company']
d_c1 = d_c.nunique()
plt.title("distribution of companies across different countries")
plt.ylabel("no of companies")
d_c1.plot(kind='bar')

# %%
# place where there are more no of companies
m_c = data1.groupby('CompanyLocation')['Company']
m_c.nunique().sort_values(ascending=False).head()

# %%
# no of chocolatebars made in a particular company in differnt countries
p_c = data[data['Company'] == 'Spencer']
p_c1 = p_c['CompanyLocation'].value_counts()
plt.title("no of chocolatebars made by SPENCER in different countries")
plt.ylabel("no of different chocolate bars made")
plt.xlabel("countries")
p_c1.plot(kind='bar')


# %%
# distribution of differnt companies in a particular location
places = ['Scotland', 'U.S.A.', 'France', 'U.K.']
plt.figure()
res = []
for i, j in zip(places, [1, 2, 3, 4]):
    com = data[data['CompanyLocation'] == i]['Company'].nunique()
    res.append(com)
print(res)
df = pd.DataFrame(res, index=places, columns=['no of companies'])
df
df.plot(kind='bar')

# %%
# which company produces more variety of chocolate bars
c_b = data.groupby('Company')['BarName']
c_b.nunique().sort_values(ascending=False).head()

# %%
# highest rated chocolate bars
data[data['Rating'] > 3]

# %%
# no of choccolate bars that were given 5/5 rating
data[data['Rating'] == 5]['BarName']

# %%
# highest rated chocolate bar in each company
h_r = data.groupby('Company')['Rating']
h_r.apply(lambda x: x.sort_values(ascending=False).nlargest(1))

# %%
# higest rated chocolate bars in those that are reviewd recentlty
re_r = data[data['ReviewDate'] == data['ReviewDate'].max()]
re_r[re_r['Rating'] == re_r['Rating'].max()]['BarName']

# %%
# countries of origin for beans
data['BeanOrigin'].nunique()
# there are 100 diferent countries where beans are produced for making bars

# %%
# which country produces more variety of beans
v_b = data.groupby('BeanOrigin')['BeanType']
v_b.nunique().sort_values(ascending=False).head(1)
# Venezuela produces 13 different kinds of beans

# %%
# which bean gives high rating
data[data['Rating'] == data['Rating'].max()]['BeanType']
# chocolate bars made using beans Trinitario &  Blend give high rating

# %%
# different beans produced across different countries
b_c = data1.groupby('BeanOrigin')['BeanType']
b_c1 = b_c.nunique()
plt.figure(figsize=(20, 5))
plt.title("different beans produced across different countries")
plt.ylabel("no of varieties")
plt.xticks(fontsize=12, rotation=90)
b_c1.plot(kind='bar')

# %%
# bar with high cocoa percent and the company which manufactures it
data[data['CocoaPercent'] == data['CocoaPercent'].max()]['Company']
# Noir Infini and Peru- Ecuador have 99% of cocoa in them


# %%
# relation b/w cocoa percent and rating
s1 = data['CocoaPercent']
s2 = []
for i in s1:
    s2.append(pd.to_numeric(i[:-1]))

rating = pd.DataFrame(data['Rating'])
cocoa = pd.DataFrame(s2, columns=['CocoaPercent'])
location = pd.DataFrame(data['CompanyLocation'])
cocoa
rati_coa = pd.merge(rating, cocoa, left_index=True, right_index=True)
rlc = pd.merge(rati_coa, location, left_index=True, right_index=True)
rlc
plt.figure(figsize=(15, 30))
x = (sns.lmplot(x='CocoaPercent', y='Rating', data=rlc, hue='CompanyLocation',
     fit_reg=False))

# %%
# % of companies in USA of the total companies
c_us = data[data['CompanyLocation'] == 'U.S.A.']['Company'].nunique()
c_total = data['Company'].nunique()
c_us / c_total * 100
# 42% of chocolatebars  made companies arepresent in USA

# %%
# Where are the best cocoa beans grown?
data.sort_values(by='Rating', ascending=False)['BeanOrigin'].head(1)

# %%
# which company produces the highest rated chocolate bars
data.sort_values(by='Rating', ascending=False)['Company'].head().unique()

# %%
# which country produces the highest rated chocolate bars
(data.sort_values(by='Rating', ascending=False)['CompanyLocation'].
 head().unique())

# %%
# Highest rated companies in each year
h = data.groupby('ReviewDate')['Rating', 'Company']
h.apply(lambda x: x.sort_values(by='Rating', ascending=False).head(1))
