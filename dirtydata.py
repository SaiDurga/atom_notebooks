"""Cleaning dirty data with pandasa and python."""

import pandas as pd

# %%
# load the dataset
data = pd.read_csv('~/data/movie_metadata.csv')

# %%
# looka at the data
data.head()
# shape of the data
data.shape
# info
data.info()
# find nulls
data.isnull().any()
# Look at the some basic stats for the ‘imdb_score’ column
data['imdb_score'].describe()
# specific column - different movie genres
data['genres']
# average duration of the movie
data['duration'].mean()
# first 10 ros of the director column
data['director_name'][:10]

# %%
# dealing with missing data
#  Add default values
 data['country'] = data['country'].fillna('')
 data['country'].isnull().any()

 # if the vales ate not strings, we can use mean, median etc to fill nans
 data['duration'] = data['duration'].fillna(data['duration'].mean())
 data['duration'].isnull().any()

# %%
# remove incomplete rows
data1 = data.dropna()
data1.shape
# the above case drops the rows even with a single nan value.

data2 = data.dropna(how='all')
data2.shape
# here rows with all nans are only dropped

# We can also put a limitation on how many non-null values need to be in a row
# in order to keep it -- use 'thresh' for it
data3 = data.dropna(thresh=5)
data3.shape

# subset
data4 = data.dropna(subset=['title_year'])
data4.shape
data['title_year'].isnull().sum()
data.shape

# %%
# Deal with error-prone columns
# Drop the columns with that are all NA values:
data5 = data.dropna(axis=1, how='all')
data5.shape

# Drop all columns with any NA values:
data6 = data.dropna(axis=1, how='any')
data6.shape
# only 9 columns are there without any nan values

# use thresh
data7 = data.dropna(axis=1, thresh=5000)
data7.shape

# use subset

# %%
# Normalize data types
data8 = pd.read_csv('~/data/movie_metadata.csv', dtype={'title_year': str})
data8.shape
data8['title_year']

# %%
# change casing
data['movie_title'].str.upper()
#to get rid of trailing whitespace
data['movie_title'].str.strip()

# %%
# rename columns
(data.rename(columns = {'title_year':'release_date',
 'movie_facebook_likes':'facebook_likes'}))

 # %%
 # to save the file
 data.to_csv('newfile.csv', encoding='utf-8')
