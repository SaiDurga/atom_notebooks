"""Generate data with submaterial id, Date, Time."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
col_names = ['Date & Time']
ti = ['0-1', '1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9', '9-10',
      '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18',
      '18-19', '19-20', '20-21', '21-22', '22-23', '23-24']

# %%
x = (pd.date_range(start=pd.datetime(2018, 1, 1), periods=44640,
     freq='min'))

df1 = pd.DataFrame()
df1['Date'] = x.date.astype('datetime64')
df1['Time'] = x.time
df1['Hour'] = df1['Time'].apply(lambda x: x.hour)
df1['Time_interval'] = [('{}-{}'.format(i, i+1)) for i in df1['Hour']]
df1['subMaterial'] = np.random.randint(1, 17, 44640)
df1['Time_interval'] = pd.Categorical(df1['Time_interval'], categories=ti,
                                      ordered=True)
df1
df1['Hour'].nunique()

# %%

df4 = (pd.DataFrame(df1.groupby(['Hour', 'subMaterial']).size(),
       columns=["no of Bags"]))
df4 = df4.reset_index()
df4
x = sns.barplot(data=df4, x='Hour', y='no of Bags', hue='subMaterial')
x.legend(bbox_to_anchor=(1.1, 1.05))
plt.title("no of bags produced for each category")

# %%
# Average no of bags produced wrt time interval
df4 = (pd.DataFrame(df1.groupby(['Time_interval', 'subMaterial']).size(),
       columns=["no of Bags"]))
df4 = df4.reset_index()
df4
plt.xticks(rotation=90)
x = sns.barplot(data=df4, x='Time_interval', y='no of Bags', hue='subMaterial')
x.legend(bbox_to_anchor=(1.1, 1.05))
plt.title("no of bags produced for each category")

# %%
# Which time period has the highest productivity
one_day = df1[df1['Date'] == '2018-01-01']
one_day['Hour'].nunique()
len(one_day)
od = pd.DataFrame(one_day.groupby('Time_interval')['subMaterial'].sum())
od.columns = ['no of bags']
od
for i in range(len(od)):
    od[i+1] = od['no of bags'][i:].cumsum()
od
x = sns.heatmap(od.iloc[:, -24:], cmap='Blues')
od.iloc[:, -24:]
