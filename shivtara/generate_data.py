"""Generate data for 1 year."""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%

d1 = pd.date_range(start='1/1/2018', periods=8760, freq='H')
d1

# %%
ti = ['0-1', '1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9', '9-10',
      '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18',
      '18-19', '19-20', '20-21', '21-22', '22-23', '23-00']
ti
len(ti)

# %%
col_names = ['Date & Time']

# %%
df = pd.DataFrame(pd.date_range(start='1/1/2018', periods=8760, freq='H'),
                  columns=col_names)
df['Date'] = [d.date() for d in df['Date & Time']]
df['Time'] = [d.time() for d in df['Date & Time']]
# df['month'] = [d.month() for d in df['Date & Time']]
df['Time_interval'] = ti * 365
df['G Maida 50Kg'] = np.random.randint(55, 68, 8760)
df['CG Maida 50Kg'] = np.nan
df['Bread Maida 50Kg'] = np.nan
df['CG Rawa 50Kg'] = np.random.randint(8, 13, 8760)
df['Bell Rawa 50Kg'] = np.nan
df['Bell chiroty 50Kg'] = np.random.randint(6, 9, 8760)
df['CG Aata 50Kg'] = np.random.randint(8, 11, 8760)
df['Bell Aata 50Kg'] = np.random.randint(8, 12, 8760)
df['G Aata 50Kg'] = np.random.randint(1, 4, 8760)
df['CG-C Aata 25Kg'] = np.nan
df['CG-C Aata 50Kg'] = np.nan
df['Bell Bran 49Kg'] = np.random.randint(10, 14, 8760)
df['CG S/D 45Kg'] = np.random.randint(1, 3, 8760)
df['Bell SF 49Kg'] = np.random.randint(11, 17, 8760)
df['CG Flakes 34Kg'] = np.random.randint(5, 16, 8760)
df['Cg Flakes 30Kg'] = np.random.randint(11, 13, 8760)
df1 = df.drop(['Date & Time', 'Time'], axis=1)
# df1 = df1.set_index(['Date', 'Time_interval'])
df1.fillna(0, inplace=True)
df1
df1['Grinding'] = (50 * df1['G Maida 50Kg']
                   + 50 * df1['CG Maida 50Kg']
                   + 50 * df1['Bread Maida 50Kg']
                   + 50 * df1['CG Rawa 50Kg']
                   + 50 * df1['Bell Rawa 50Kg']
                   + 50 * df1['Bell chiroty 50Kg']
                   + 50 * df1['CG Aata 50Kg']
                   + 50 * df1['Bell Aata 50Kg']
                   + 50 * df1['G Aata 50Kg']
                   + 25 * df1['CG-C Aata 25Kg']
                   + 50 * df1['CG-C Aata 50Kg']
                   + 49 * df1['Bell Bran 49Kg']
                   + 45 * df1['CG S/D 45Kg']
                   + 49 * df1['Bell SF 49Kg']
                   + 34 * df1['CG Flakes 34Kg']
                   + 30 * df1['Cg Flakes 30Kg'])
df1
df1['Time_interval'] = pd.Categorical(df['Time_interval'], categories=ti,
                                      ordered=True)

# %%
# production(kgs) in seasons
d1 = df1.groupby(df['Date'].apply(lambda x: x.month) // 4.5 + 1)
grind_sum = d1['Grinding'].sum()
grind_sum = grind_sum.reset_index()
grind_sum.columns = ['Season', 'Grinding']

# %%
# production in seasons
grind_sum
x = sns.barplot(data=grind_sum, x='Season', y='Grinding')
plt.title("production in different seasons")

# %%
grind_avg = d1['Grinding'].sum() / d1.size()
grind_avg = grind_avg.reset_index()
grind_avg.columns = ['Season', 'Grinding']
x = sns.barplot(data=grind_avg, x='Season', y='Grinding')
plt.title("average production in different seasons")

# %%
df2 = df1.sum().reset_index()
df2.columns = ['Type', 'Production']
df2 = df2[: -1]
df2
plt.xticks(rotation=90)
x = sns.barplot(data=df2, x='Type', y='Production')

# %%
# Average no of bags for hour per day
hr = df1.groupby('Time_interval')
hr1 = hr['Grinding'].sum()/hr.size()
hr1 = hr1.reset_index()
hr1.columns = ['Time_interval', 'Grinding']
hr1
plt.xticks(rotation=90)
x = sns.barplot(data=hr1, x='Time_interval', y='Grinding')

# %%
# Average bags produced per day for each month
df3 = df1.groupby('Date').sum()
df3 = df3.drop('Grinding', axis=1)
df3


def total(data):
    """Add elements in a row."""
    res = []
    for i in range(len(data)):
        res.append(data.iloc[i].sum())
    return res


df3['total'] = total(df3)
df3 = df3.reset_index()
df3_g = df3.groupby(df3['Date'].apply(lambda x: x.month))
df3_a = df3_g['total'].sum()/df3_g.size()
df3_a = df3_a.reset_index()
df3_a.columns = ['Month', 'avg_no_of_bags']
df3_a
x = sns.barplot(data=df3_a, x='Month', y='avg_no_of_bags')
plt.title("Average no of bags produced per day in each month")

# %%
# Production(total no of bags) per season
df3
df3_s = df3.groupby(df3['Date'].apply(lambda x: x.month) // 4.5 + 1)
pro = df3_s['total'].sum()
pro
pro = pro.reset_index()
pro.columns = ['Season', 'no_of_bags']
pro
x = sns.barplot(data=pro, x='Season', y='no_of_bags')
plt.title("Total no of bags produced per season")

# %%
# Average no of bags per season
df3_s = df3_s['total'].sum()/df3_s.size()
df3_s = df3_s.reset_index()
df3_s.columns = ['Season', 'avg_no_of_bags']
df3_s
x = sns.barplot(data=df3_s, x='Season', y='avg_no_of_bags')
plt.title("Average no of bags produced per season")

# %%
