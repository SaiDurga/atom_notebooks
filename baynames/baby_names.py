
"""US baby names."""

# %%
# import libraries and load the dataset
import pandas as pd
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/NationalNames.csv')
data.head()
data.shape

# %%
# find out if there are any nulls
data.isnull().any()   # no nulls

# %%
# info
data.info()

# %%
# Range of years for which the data is collected
data['Year'].min(), data['Year'].max()  # (1880, 2014)

# %%
# no of girls and boys names in the dataset
data.groupby('Gender')['Name'].nunique()

# %%
# Top most popular names
(data.sort_values(by='Count', ascending=False).groupby('Name').
 sum().sort_values(by='Count', ascending=False).head(10))

# %%
# Top less popular names
(data.sort_values(by='Count').groupby('Name').
 sum().sort_values(by='Count').head(10))

# %%
# average length of the name for girls
data[data['Gender'] == 'F']['Name'].apply(len).mean()

# %%
# average length of the name for boys
data[data['Gender'] == 'M']['Name'].apply(len).mean()

# %%
# How does the name James vary across different years
j = data[data['Name'] == 'James'].groupby('Year')['Count'].sum()
plt.plot(j)

# %%
# percetage of names not even used 10 times
less = data.groupby('Name')['Count'].sum()
len(less[less < 10])/len(data)*100

# %%
# with which letter most of the names start with
data['Name'].apply(lambda x: x[0]).value_counts().sort_values(ascending=False)

# %%
# common names b/w boys and girls


def common_ele(lst1, lst2):
    """Find common elements in lists given."""
    res = []
    for e in lst1:
        if e in lst2:
            res.append(e)
    return res


n1 = data[data['Gender'] == 'F']['Name'].unique()
n2 = data[data['Gender'] == 'M']['Name'].unique()
common = common_ele(n1, n2)
len(common)


# %%
# find the names of female that start with S
sw = data[data['Gender'] == 'F']['Name']
len(sw[sw.str.startswith(pat='S')])

# %%
# top female names in the year 2010
d2010 = data[data['Year'] == 2010]
(d2010[d2010['Gender'] == 'F'].groupby('Name')['Count'].
 sum().sort_values(ascending=False).head())

# %%
# different names for every year(plot)
n = data.groupby('Year')['Name'].nunique()
plt.plot(n)

# %%
# No of times a particular name is repeated(Mary)
data[data['Name'] == 'Mary']['Count'].sum()

# %%
# no of babies born each year
data.groupby('Year')['Count'].sum()

# %%
# no of girls and boys each year
data.groupby(['Year', 'Gender'])['Count'].sum()
