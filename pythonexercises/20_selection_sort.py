"""Sort the elements in a list using selection sort."""


def sel_sort(lst):
    """Sort the elemnts in a list."""
    if len(lst) == 0:
        return None
    for i in range(len(lst)-1, 0, -1):
        j = lst.index(max(lst[:i+1]))
        lst[i], lst[j] = lst[j], lst[i]
    return lst


def test_sel_sort1():
    """Test case for selection sort."""
    assert sel_sort([1, 4, 3, 5, 6, 8]) == [1, 3, 4, 5, 6, 8]


def test_sel_sort2():
    """Test case with repeated elements."""
    assert sel_sort([1, 4, 3, 4, 6, 3]) == [1, 3, 3, 4, 4, 6]


def test_sel_sort3():
    """Test case with negative values."""
    assert sel_sort([1, -4, 3, -5, 6, -8]) == [-8, -5, -4, 1, 3, 6]


def test_sel_sort4():
    """Test case with single element."""
    assert sel_sort([1]) == [1]


def test_sel_sort5():
    """Test case with empty list."""
    assert sel_sort([]) is None
