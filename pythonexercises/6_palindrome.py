"""Palindrome."""


def reverse_list(lst):
    """Reverse the elements in a given list."""
    res = []
    length = len(lst)
    for i in range(length, 0, -1):
        res.append(lst[i-1])
    return res


def palindrome(lst):
    """Verify whether the given list is palindrome or not."""
    if len(lst) != 0:
        if lst is reverse_list(lst):
            return True
        return False
    return False


palindrome([1])


def test_palindrome1():
    """Test cases for palindrome."""
    palindrome([1, 2, 3, 4, 3, 2, 1]) is True


def test_palindrome2():
    """Test cases for palindrome."""
    palindrome([1.0, 2.1, 3.3, 4.8, 3.3, 2.1, 1.0]) is True


def test_palindrome3():
    """Test cases for palindrome."""
    palindrome(['a', 'b', 'c', 'b', 'a']) is True


def test_palindrome4():
    """Test cases for palindrome."""
    palindrome(['a', 'b', 1, 1,  'b', 'a']) is True


def test_palindrome5():
    """Test cases for palindrome."""
    palindrome([23, 'a', 'b', 68.63, 'e', 68.63, 'b', 'a', 23]) is True


def test_palindrome6():
    """Test cases for palindrome."""
    palindrome([1, 2, 3, 4, 5, 3, 2, 1]) is False


def test_palindrome7():
    """Test cases for palindrome."""
    palindrome([1.0, 2.1, 3.3, 4.8, 8.9, 3.3, 2.1, 1.0]) is False


def test_palindrome8():
    """Test cases for palindrome."""
    palindrome(['a', 'b', 'c', 'd', 'b', 'a']) is False


def test_palindrome9():
    """Test cases for palindrome."""
    palindrome(['a', 'b', 1, 1,  'a', 'b', 'a']) is False


def test_palindrome10():
    """Test cases for palindrome."""
    (palindrome([23, 'a', 'b', 68.63, 'e', 12, 68.63, 'b', 'a', 23]) is
     False)


def test_palindrome11():
    """Test case with empty list."""
    palindrome([]) is False


def test_palindrome12():
    """Test acse with single elemnt in the list."""
    palindrome([1]) is True


def test_palindrome13():
    """Test case with two same elemnts."""
    palindrome([1, 1]) is True


def test_palindrome14():
    """Test case with two different elemnts."""
    palindrome([1, 2]) is False
