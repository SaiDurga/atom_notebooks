"""Uncompressed version."""


def uncompress(lst):
    """Uncompress a compressed list."""
    res = []
    if len(lst) != 0:
        for e in lst:
            if type(e) == list:
                res += ([e[1]] * e[0])
            else:
                res.append(e)
        return res
    return None


def test_uncompress1():
    """Test case for uncompress."""
    assert uncompress([[2, 2], 3, [2, 5], [3, 3]]) == [2, 2, 3, 5, 5, 3, 3, 3]


def test_uncompress2():
    """Test case for uncompress with single list element."""
    assert uncompress([[2, 2]]) == [2, 2]


def test_uncompress3():
    """Test case for uncompress with uncompressed file itself."""
    assert uncompress([1, 2, 3, 4, 5, 6]) == [1, 2, 3, 4, 5, 6]


def test_uncompress4():
    """Test case for uncompress with single element."""
    assert uncompress([3]) == [3]


def test_uncompress5():
    """Test case for uncompress with empty list."""
    assert uncompress([]) is None
