"""using pytest for testing."""


def last_element(lst):
    """Find last elemnt in a list."""
    if len(lst) != 0:
        if len(lst) == 1:
            return lst[0]
        return last_element(lst[1:])
    return 'no elements'


def test_last_element1():
    """Test case."""
    assert last_element([1, 2, 3, 4, 5]) == 5


def test_last_element2():
    """Test case."""
    assert last_element([1.3, 3.5, 5.7, 7.8]) == 7.8


def test_last_element3():
    """Test case."""
    assert last_element([1, 3.5, 5.6, 67, 63, 68.9, 10.0, 32, 32, 0.1]) == 0.1


def test_last_element4():
    """Test case."""
    assert last_element(['a', 'b', 'c', 'd', 'e']) == 'e'


def test_last_element5():
    """Test case."""
    assert last_element(['a', 1, 'b', 'c', 2, 3]) == 3


def test_last_element6():
    """Test case."""
    assert last_element(['a', 1, 'b', 'c', 2, 3, 'd']) == 'd'


def test_last_element7():
    """Test case."""
    assert last_element([1, 3.5, 'a', 'b', 's', 2.8, 2, 'k']) == 'k'


def test_last_element8():
    """Test case."""
    assert last_element([12000, 1800, 17864, 123.45]) == 123.45


def test_last_element9():
    """Test case."""
    assert last_element(['abc', 'def', 'mno']) == 'mno'


def test_last_element10():
    """Test case."""
    assert last_element([1, 1.8, 18, 1800, 'a', 'abc']) == 'abc'


def test_last_element11():
    """Test case with empty list."""
    last_element([]) == 'no elememts'


def test_last_element12():
    """Test case with single element."""
    assert last_element([1]) == 1


# 10 passed in 0.08 seconds
