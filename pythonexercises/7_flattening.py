"""Flattening a nested lsit."""


def flattening(lst):
    """Flatten a nested list structure."""
    res = []
    if len(lst) != 0:
        for e in lst:
            if type(e) == list:
                res.extend(flattening(e))
            else:
                res.append(e)
        return res
    return None


def test_flatten1():
    """Test case for flattening."""
    assert (flattening(['a', ['b', ['c', 'd'], 'e']]) ==
            ['a', 'b', 'c', 'd', 'e'])


def test_flatten2():
    """Test case for flattening."""
    assert (flattening([['a', 'b'], 'c', ['d', ['e', 'f'], 'g']]) ==
            ['a', 'b', 'c', 'd', 'e', 'f', 'g'])


def test_flatten3():
    """Test case for flattening."""
    assert (flattening([1, [2, [3, 4], 5]]) ==
            [1, 2, 3, 4, 5])


def test_flatten4():
    """Test case for flattening."""
    assert (flattening([1.2, [2.3, [3.4, 4.5], 5.6]]) ==
            [1.2, 2.3, 3.4, 4.5, 5.6])


def test_flatten5():
    """Test case for flattening."""
    assert (flattening(['a', [1, ['c', 2.5], 'e']]) ==
            ['a', 1, 'c', 2.5, 'e'])


def test_flatten6():
    """Test case with flattened list."""
    assert flattening([1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_flatten7():
    """test case with single element."""
    assert flattening([1]) == [1]


def test_flatten8():
    """Test case with empty list."""
    assert flattening([]) is None
