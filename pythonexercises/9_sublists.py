"""Collect consecutive duplicates of list elements into sublists."""


def sublists(lst):
    """Collect consecutive duplicates of list elements into sublists."""
    res = []
    res1 = []
    if len(lst) != 0:
        res.append(lst[0])
        for i in range(1, len(lst)):
            if lst[i] == lst[i-1]:
                res.append(lst[i])
            else:
                res1.append(res)
                return res1 + sublists(lst[i:])
        return res1 + [res]
    return None


    sublists([68, 68, 68, 68])


def test_sublist1():
    """Test case for sublists."""
    assert sublists([1, 1, 1, 2, 2, 3]) == [[1, 1, 1], [2, 2], [3]]


def test_sublist2():
    """Test case for sublists."""
    assert (sublists([1.2, 1.3, 1.3, 2.5, 2.5, 3.6]) ==
            [[1.2], [1.3, 1.3], [2.5, 2.5], [3.6]])


def test_sublist3():
    """Test case for sublists."""
    assert (sublists(['a', 'a', 'b', 'b', 'c']) ==
            [['a', 'a'], ['b', 'b'], ['c']])


def test_sublist4():
    """Test case for sublists."""
    assert (sublists([1, 1.2, 1.2, 2, 2, 3.6]) ==
            [[1], [1.2, 1.2], [2, 2], [3.6]])


def test_sublist5():
    """Test case for sublists."""
    assert (sublists([1, 'a', 'a', 'b', 2, 2]) ==
            [[1], ['a', 'a'], ['b'], [2, 2]])


def test_sublist6():
    """Test case for sublists with all different elemnts."""
    assert sublists([1, 2, 3, 4, 5]) == [[1], [2], [3], [4], [5]]


def test_sublist7():
    """Test case for sublists with a ll similar elements."""
    assert sublists([68, 68, 68, 68]) == [[68, 68, 68, 68]]


def test_sublist8():
    """Test case for sublists with empty list."""
    assert sublists([]) is None


# 5 passed in 0.01 seconds
