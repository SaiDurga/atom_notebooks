"""Duplicate the elemnts of a list."""


def duplicate2(lst):
    """Duplicate the elements."""
    res = []
    if len(lst) != 0:
        for e in lst:
            res += ([e] * 2)
        return res
    return None


duplicate2([])


def test_duplicate2_1():
    """Test case for creating duplicates."""
    assert duplicate2([1, 2, 3, 4]) == [1, 1, 2, 2, 3, 3, 4, 4]


def test_duplicate2_2():
    """Test case with all similar elements."""
    assert duplicate2([1, 1, 1, 1]) == [1, 1, 1, 1, 1, 1, 1, 1]


def test_duplicate2_3():
    """Test case with single element."""
    assert duplicate2([1]) == [1, 1]


def test_duplicate2_4():
    """Test case for creating duplicates."""
    assert (duplicate2(['a', 'b', 'c', 'd']) ==
            ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd'])


def test_duplicate2_5():
    """Test case with empty list."""
    assert duplicate2([]) is None
