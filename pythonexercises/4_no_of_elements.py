"""Find no of elemnts in a given list."""


def elements(lst):
    """Find no of elemnts in a given list."""
    if len(lst) != 0:
        return 1 + elements(lst[1:])
    return 0


def test_elements1():
    """Test case for no of elemnts in a list."""
    assert elements([1, 2, 3, 4, 5]) == 5


def test_elements2():
    """Test case for no of elemnts in a list."""
    assert elements([1.3, 3.5, 5.7, 7.8]) == 4


def test_elements3():
    """Test case for no of elemnts in a list."""
    assert elements([1, 3.5, 5.6, 67, 63, 68.9, 10.0, 32, 32, 0.1]) == 10


def test_elements4():
    """Test case for no of elemnts in a list."""
    assert elements(['a', 'b', 'c', 'd', 'e']) == 5


def test_elements5():
    """Test case for no of elemnts in a list."""
    assert elements(['a', 1, 'b', 'c', 2, 3]) == 6


def test_elements6():
    """Test case for no of elemnts in a list."""
    assert elements(['a', 1, 'b', 'c', 2, 3, 'd']) == 7


def test_elements7():
    """Test case for no of elemnts in a list."""
    assert elements([1, 3.5, 'a', 'b', 's', 2.8, 2, 'k']) == 8


def test_elements8():
    """Test case for no of elemnts in a list."""
    assert elements([12000, 1800, 17864, 123.45]) == 4


def test_elements9():
    """Test case for no of elemnts in a list."""
    assert elements(['abc', 'def', 'mno']) == 3


def test_elements10():
    """Test case for no of elemnts in a list."""
    assert elements([1, 1.8, 18, 1800, 'a', 'abc']) == 6


def test_elements11():
    """Test case with empty list."""
    assert elements([]) == 0


def test_elements12():
    """Test case with single element."""
    assert elements([1]) == 1
