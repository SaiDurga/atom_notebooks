"""Reverse a list."""


def reverse_list(lst):
    """Reverse the elements in a given list."""
    res = []
    length = len(lst)
    for i in range(length, 0, -1):
        res.append(lst[i-1])
    return res


def test_reverse1():
    """Test case for reversing the elemnts of a list."""
    assert reverse_list([1, 2, 3, 4, 5, 6]) == [6, 5, 4, 3, 2, 1]


def test_reverse2():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list([1.0, 1.8, 0.5, 23.1, 32.25, 68.63]) ==
            [68.63, 32.25, 23.1, 0.5, 1.8, 1.0])


def test_reverse3():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list([1, 3.5, 5.6, 67, 63, 68.9, 10.0, 32, 32, 0.1]) ==
            [0.1, 32, 32, 10.0, 68.9, 63, 67, 5.6, 3.5, 1])


def test_reverse4():
    """Test case for reversing the elemnts of a list."""
    assert reverse_list(['a', 'b', 'c', 'd', 'e']) == ['e', 'd', 'c', 'b', 'a']


def test_reverse5():
    """Test case for reversing the elemnts of a list."""
    assert reverse_list(['a', 1, 'b', 'c', 2, 3]) == [3, 2, 'c', 'b', 1, 'a']


def test_reverse6():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list(['a', 1, 'b', 'c', 2, 3, 'd']) ==
            ['d', 3, 2, 'c', 'b', 1, 'a'])


def test_reverse7():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list([1, 3.5, 'a', 'b', 's', 2.8, 2, 'k']) ==
            ['k', 2, 2.8, 's', 'b', 'a', 3.5, 1])


def test_reverse8():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list([12000, 1800, 17864, 123.45]) ==
            [123.45, 17864, 1800, 12000])


def test_reverse9():
    """Test case for reversing the elemnts of a list."""
    assert reverse_list(['abc', 'def', 'mno']) == ['mno', 'def', 'abc']


def test_reverse10():
    """Test case for reversing the elemnts of a list."""
    assert (reverse_list([1, 1.8, 18, 1800, 'a', 'abc']) ==
            ['abc', 'a', 1800, 18, 1.8, 1])


def test_reverse11():
    """Test case with empty list."""
    assert reverse_list([]) == []


def test_reverse12():
    """Test case with single element."""
    assert reverse_list([1]) == [1]
