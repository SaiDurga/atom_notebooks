"""Encode consecutive duplicates of elements as lists."""


def sublists(lst):
    """Collect consecutive duplicates of list elements into sublists."""
    res = []
    res1 = []
    if len(lst) != 0:
        res.append(lst[0])
        for i in range(1, len(lst)):
            if lst[i] == lst[i-1]:
                res.append(lst[i])
            else:
                res1.append(res)
                return res1 + sublists(lst[i:])
        return res1 + [res]
    return None


def compression(lst):
    """Encode consecutive duplicates of elements as lists."""
    res = sublists(lst)
    res1 = []
    if len(lst) != 0:
        for e in res:
            res1.append([len(e), e[0]])
        return res1
    return None


compression([1])


def test_compress1():
    """Test case for compression."""
    assert compression([1, 1, 1, 2, 2, 3]) == [[3, 1], [2, 2], [1, 3]]


def test_compress2():
    """Test case for compression."""
    assert (compression([1.2, 1.3, 1.3, 2.5, 2.5, 3.6]) ==
            [[1, 1.2], [2, 1.3], [2, 2.5], [1, 3.6]])


def test_compress3():
    """Test case for compression."""
    assert (compression(['a', 'a', 'b', 'b', 'c']) ==
            [[2, 'a'], [2, 'b'], [1, 'c']])


def test_compress4():
    """Test case for compression."""
    assert (compression([1, 1.2, 1.2, 2, 2, 3.6]) ==
            [[1, 1], [2, 1.2], [2, 2], [1, 3.6]])


def test_compresst5():
    """Test case for compression."""
    assert (compression([1, 'a,', 'a', 'b', 2, 2]) ==
            [[1, 1], [1, 'a,'], [1, 'a'], [1, 'b'], [2, 2]])


def test_compress6():
    """Test case for compression with two differen elements."""
    assert compression([1, 2]) == [[1, 1], [1, 2]]


def test_compress7():
    """Test case for compression with two same elements."""
    assert compression([1, 1]) == [[2, 1]]


def test_compress8():
    """Test case for compression with empty list."""
    assert compression([]) is None


def test_compress9():
    """Test case for compression with single element."""
    assert compression([1]) == [[1, 1]]




# 5 passed in 0.01 seconds
