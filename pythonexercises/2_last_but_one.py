"""find the last but one elemnt ina a given list."""


def last_but_one(lst):
    """Find last but one element in a given list."""
    if len(lst) != 0:
        if len(lst) == 1:
            return 'single elment in the list'
        elif len(lst) == 2:
            return lst[0]
        return last_but_one(lst[1:])
    return 'emptylist given'


def test_lastbutone1():
    """Testcase for lastbut one element."""
    assert last_but_one([1, 2, 3, 4, 5]) == 4


def test_lastbutone2():
    """Testcase for lastbut one element."""
    assert last_but_one([1.3, 3.5, 5.7, 7.8]) == 5.7


def test_lastbutone3():
    """Testcase for lastbut one element."""
    assert last_but_one([1, 3.5, 5.6, 67, 63, 68.9, 10.0, 32, 32, 0.1]) == 32


def test_lastbutone4():
    """Testcase for lastbut one element."""
    assert last_but_one(['a', 'b', 'c', 'd', 'e']) == 'd'


def test_lastbutone5():
    """Testcase for lastbut one element."""
    assert last_but_one(['a', 1, 'b', 'c', 2, 3]) == 2


def test_lastbutone6():
    """Testcase for lastbut one element."""
    assert last_but_one(['a', 1, 'b', 'c', 2, 3, 'd']) == 3


def test_lastbutone7():
    """Testcase for lastbut one element."""
    assert last_but_one([1, 3.5, 'a', 'b', 's', 2.8, 2, 'k']) == 2


def test_lastbutone8():
    """Testcase for lastbut one element."""
    assert last_but_one([12000, 1800, 17864, 123.45]) == 17864


def test_lastbutone9():
    """Testcase for lastbut one element."""
    assert last_but_one(['abc', 'def', 'mno']) == 'def'


def test_lastbutone10():
    """Testcase for lastbut one element."""
    assert last_but_one([1, 1.8, 18, 1800, 'a', 'abc']) == 'a'


def test_lastbutone11():
    """Testcase with empty list."""
    last_but_one([]) == 'emptylist given'


def test_lastbutone12():
    """Testcase with empty list."""
    last_but_one([]) == 'single elment in the list'
