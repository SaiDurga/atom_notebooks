"""Sort the elements in a list using bubblesort."""


def b_sort(lst):
    """Sort the elements in a list."""
    if len(lst) != 0:
        for i in range(len(lst)-1):
            for j in range(i + 1, len(lst)):
                if (lst[i] > lst[j]):
                    (lst[i], lst[j]) = (lst[j], lst[i])
        return lst
    return None


b_sort([1, -3, 2, -1, 8, 7, -5])


def test_b_sort1():
    """Test case for bublle sort."""
    assert b_sort([1, 2, 4, 6, 5, 10, 8]) == [1, 2, 4, 5, 6, 8, 10]


def test_b_sort2():
    """Test case with repeated elements."""
    assert b_sort([1, 2, 4, 6, 5, 4, 5]) == [1, 2, 4, 4, 5, 5, 6]


def test_b_sort3():
    """Test case with single element."""
    assert b_sort([]) is None


def test_b_sort4():
    """Test case with single element."""
    assert b_sort([1]) == [1]


def test_b_sort5():
    """Test case with negative elements."""
    assert b_sort([1, -3, 2, -1, 8, 7, -5]) == [-5, -3, -1, 1, 2, 7, 8]
