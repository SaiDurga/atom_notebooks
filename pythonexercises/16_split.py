"""Split a list into two parts, the length of the first part is given."""

def split(lst, n):
    """Split a list into two parts."""
    if len(lst) != 0:
        if len(lst) > n & n != 0:
            return [lst[:n]] + [lst[n:]]
        return lst
    return None

    split([1, 2, 3, 4, 5, 6], 3)


def test_split1():
    """Test case for split."""
    assert split([1, 2, 3, 4, 5, 6], 3) == [[1, 2, 3], [4, 5, 6]]


def test_split2():
    """Test case for split with n > length of list. No splitting occurs. """
    assert split([1, 2, 3, 4], 6) == [1, 2, 3, 4]


def test_split3():
    """Test case for split with n =0. No splitting occurs."""
    assert split([1, 2, 3, 4, 5], 0) == [1, 2, 3, 4, 5]


def test_split4():
    """Test case for split with n = length of the list. No splitting occurs."""
    assert split([1, 2], 2) == [1, 2]


def test_split5():
    """Test case for split."""
    assert split([], 3) is None
