"""Replicate the elements of a list for a given no of times."""


def replicate(lst, n):
    """Replicate the elemnts of a list."""
    res = []
    if (len(lst) != 0) & (n != 0):
        for e in lst:
            res += ([e] * n)
        return res
    return None


replicate([], 1)


def test_replicate1():
    """Test case for replicate."""
    assert replicate([1, 2, 3], 3) == [1, 1, 1, 2, 2, 2, 3, 3, 3]


def test_replicate2():
    """Test case for replicate."""
    assert replicate(['a', 'b', 'c'], 2) == ['a', 'a', 'b', 'b', 'c', 'c']


def test_replicate3():
    """Test case with single element."""
    assert replicate([1], 2) == [1, 1]


def test_replicate4():
    """Test case with single element."""
    assert replicate([1], 1) == [1]


def test_replicate5():
    """Test case with single element."""
    assert replicate([1], 0) is None


def test_replicate6():
    """Test case with single element."""
    assert replicate([], 5) is None
