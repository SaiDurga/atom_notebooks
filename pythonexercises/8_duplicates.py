"""Eliminate consecutive duplicates in a list."""


def duplicate(lst):
    """Eliminating consecutive duplicates."""
    res = []
    if len(lst) != 0:
        res.append(lst[0])
        for i in range(1, len(lst)):
            if lst[i-1] != lst[i]:
                res.append(lst[i])
        return res
    return None


def test_duplicates1():
    """Test case for duplicates."""
    assert duplicate([1, 2, 3, 1, 1, 2, 3]) == [1, 2, 3, 1, 2, 3]


def test_duplicates2():
    """Test case for duplicates."""
    assert (duplicate(['a', 'b', 'a', 'a', 'b', 'b', 'c']) ==
            ['a', 'b', 'a', 'b', 'c'])


def test_duplicates3():
    """Test case for duplicates."""
    assert (duplicate([1.2, 2.3, 3.3, 1.8, 1.8, 2.3, 3.4]) ==
            [1.2, 2.3, 3.3, 1.8, 2.3, 3.4])


def test_duplicates4():
    """Test case for duplicates."""
    assert duplicate([1, 2, 2, 'a', 'a', 1, 'a']) == [1, 2, 'a', 1, 'a']


def test_duplicates5():
    """Test case for duplicates."""
    assert (duplicate([1.2, 1.2, 'a', 1, 'a', 'a', 1.2]) ==
            [1.2, 'a', 1, 'a', 1.2])

def test_duplicates6():
    """Test case for duplicates with single element."""
    assert duplicate([1]) == [1]


def test_duplicates7():
    """Test case for duplicates with all similar elements."""
    assert duplicate([1, 1, 1, 1]) == [1]


def test_duplicates8():
    """Test case for duplicates with empty list"""
    assert duplicate([]) is None
