"""Binary search to find a number in a sorted list."""


def b_s(lst, n, i1, i2):
    """Try binary search."""
    if len(lst) != 0:
        m = (i1+i2)//2
        if lst[m] == n:
            return m
        if lst[m] > n:
            return b_s(lst, n, i1=i1, i2=m-1)
        return b_s(lst, n, i1=m+1, i2=i2)
    return None


lst = [2, 3, 4, 5, 8, 10, 12]
b_s(lst, 2, 0, len(lst)-1)


def test_b_s1():
    """Test case for binary search."""
    lst = [1, 2, 3, 4, 5]
    assert b_s(lst, 2, 0, len(lst)-1) == 1


def test_b_s2():
    """Test case to find last element."""
    lst = [1, 2, 3, 4, 5]
    assert b_s(lst, 5, 0, len(lst)-1) == 4


def test_b_s3():
    """Test case with single element."""
    lst = [2]
    assert b_s(lst, 2, 0, len(lst)-1) == 0


def test_b_s4():
    """Test case for binary search."""
    lst = []
    assert b_s(lst, 2, 0, len(lst)-1) is None


# def test_b_s5():
#    """Test case if the element is not in the list."""
#    lst = [1, 2, 3, 4, 5]
#    assert b_s(lst, 10, 0, len(lst)-1) is None
