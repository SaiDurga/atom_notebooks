"""Binary search using loops in python."""


def b_s2(lst, n):
    """Binary search using loops."""
    i1 = 0
    i2 = len(lst) - 1
    if len(lst) != 0:
        while(i1 <= i2):
            m = (i1+i2)//2
            if lst[m] == n:
                return m
            elif lst[m] > n:
                i2 = m-1
            else:
                i1 = m+1
        return None
    return None


def test_b_s21():
    """Test case for binary search."""
    assert b_s2([1, 3, 5, 7, 8, 9, 10], 5) == 2


def test_b_s22():
    """Test case to find last element."""
    assert b_s2([1, 3, 5, 7, 8, 9, 10], 10) == 6


def test_b_s23():
    """Test case with single element."""
    assert b_s2([2], 2) == 0


def test_b_s24():
    """Test case for binary search."""
    assert b_s2([], 2) is None


def test_b_s25():
    """Test case if the element is not in the list."""
    assert b_s2([1, 3, 5, 8, 12, 14, 16], 10) is None
