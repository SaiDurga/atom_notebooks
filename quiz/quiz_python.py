"""Implement quiz maker in python."""

import random as rn
from itertools import combinations
import re
import collections


def read_file(path):
    """Read the file from the given path."""
    with open(path) as t:
        test = t.read()
    test = test.splitlines()
    test1 = []
    for i in test:
        test1.append(i.split(','))
    return test1


def select_ques(test1):
    """Select 6 questions."""
    comb = []
    for i in combinations(range(30), 6):
        sum_cat = 0
        sum_diff = 0
        for j in i:
            sum_cat += int(test1[j][1])
            sum_diff += int(test1[j][2])
            if sum_cat == 32:
                if sum_diff == 32:
                    comb.append(list(i))
    index = rn.randint(0, len(comb))
    ques_no = comb[index]
    return ques_no


def save_ques(ques_no, test1):
    """Display questions and store results."""
    Que = dict()
    Opt = dict()
    c_a = dict()
    cat = dict()
    f = open('/home/saidurga/python/atom_notebooks/quiz/test.txt', 'w')
    for i in range(len(ques_no)):
        Que[i] = test1[ques_no[i]][0]
        f.write('{})).{}'.format(i+1, test1[ques_no[i]][0]))
        f.write("\n")
        Opt[i] = test1[ques_no[i]][4:8]
        f.write('1.{}'.format(test1[ques_no[i]][4]))
        f.write("\n")
        f.write('2.{}'.format(test1[ques_no[i]][5]))
        f.write("\n")
        f.write('3.{}'.format(test1[ques_no[i]][6]))
        f.write("\n")
        f.write('4.{}'.format(test1[ques_no[i]][7]))
        f.write("\n")
        f.write("\n")
        c_a[i] = int(test1[ques_no[i]][3])
        cat[i] = int(test1[ques_no[i]][1])
    f.close()
    return Que, Opt, c_a, cat


def start_test(Que, Opt, c_a, cat):
    """Start test."""
    y_a = dict()
    n = 0
    print("Option indications")
    op1 = input("for first option:  ")
    op2 = input("for second option:  ")
    op3 = input("for third option:  ")
    op4 = input("for fourth option:  ")
    while n <= 5:
        if cat[n] == 1:
            print("========GAMES AND SPORTS=========")
        elif cat[n] == 5:
            print("=========INDIAN HISTORY===========")
        else:
            print("=========TELUGU MOVIES============")
        print(Que[n])
        print('{}.{}'.format(op1, Opt[n][0]))
        print('{}.{}'.format(op2, Opt[n][1]))
        print('{}.{}'.format(op3, Opt[n][2]))
        print('{}.{}'.format(op4, Opt[n][3]))
        a = input("ENTER YOUR ANSWER: ")
        while not re.findall(r'[1-4pn]', a) or len(a) != 1:
            print("enter a valid option between 1-4")
            a = input("ENTER YOUR ANSWER: ")
        if re.findall(r'[1-4]', a):
            y_a[n] = int(a)
            n += 1
        elif re.findall(r'[n]', a):
            n += 1
        else:
            n -= 1
    y_a = collections.OrderedDict(sorted(y_a.items()))
    return y_a


def display_result(c_a, y_a):
    """Displaycorrect answer and answer given for each question."""
    print("#########################YOUR RESULT##############################")
    print('QUESTION_NO', 'CORRECT_ANSWER', 'YOUR_ANSWER', sep='\t\t\t')
    for i in range(6):
        q_no = i+1
        cor_ans = list(c_a.values())[i]
        your_ans = list(y_a.values())[i]
        print(q_no, cor_ans, your_ans, sep=' \t\t\t\t ')


def display_score(c_a, y_a):
    """Display score for the quiz."""
    score = 0
    for i in range(len(list(y_a.values()))):
        if list(y_a.values())[i] == list(c_a.values())[i]:
            score += 1
    print("your score is {}/6".format(score))


# %%


def main():
    """Write main function."""
    path = '/home/saidurga/python/atom_notebooks/quiz/questions.txt'
    test1 = read_file(path)
    ques_no = select_ques(test1)
    Que, Opt, c_a, cat = save_ques(ques_no, test1)
    y_a = start_test(Que, Opt, c_a, cat)
    display_result(c_a, y_a)
    display_score(c_a, y_a)


main()
