"""Quiz."""
import pandas as pd
import numpy as np
from itertools import combinations
import random as rn

data = pd.read_csv('~/python/atom_notebooks/quiz/quiz.csv')
x1 = pd.DataFrame()
for i in combinations(data.index, 6):
    sum1 = (data.loc[i, 'Difficulty_level'].sum())
    sum2 = (data.loc[i, 'Category'].sum())
    x1 = pd.concat([x1, pd.Series([i, sum1, sum2],
                   index=['comb', 'diff_sum', 'cat_sum'])], axis=1)
x1 = x1.T.reset_index(drop=True)
x2 = x1[(x1['diff_sum'] == 32) & (x1['cat_sum'] == 32)]
x2 = x2.reset_index(drop=True)
comb_ind = rn.randint(0, len(x2)-1)
index = x2.loc[comb_ind, 'comb']
out = data.loc[list(index)]
Answers = []
for i, j in zip(out['Question'], out['Options']):
    print(i)
    print(j)
    Answers.append(int(input("ENTER YOUR ANSWER")))
d1 = pd.DataFrame({'Question_no': [1, 2, 3, 4, 5, 6],
                  'correct_answer': out['KEY'], 'your_answer': Answers})
res = np.sum(d1['correct_answer'] == d1['your_answer'])
print(d1.to_string(index=False))
print("Your score is {}/6".format(res))
