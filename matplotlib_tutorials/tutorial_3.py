"""Matplotlib practice tutorial-3."""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cycler import cycler

# %%
# to use any style use plt.style.use(<stylename>)
plt.style.use('ggplot')

# %%
data = np.random.randn(50)

# %%
# list all the different styles available
print(plt.style.available)

# %%
# different style can be used at a time by passing them all in a list
plt.style.use(['dark_background', 'seaborn'])

# %%
# Temporary styles - the style package provides a context manager for limiting
# your changes to a specific scope
with plt.style.context(('dark_background')):
    plt.plot(np.sin(np.linspace(0, 2 * np.pi)), 'r-o')
plt.show()

# matplotlib rcParams
# Dynamic rc settings
# %%
mpl.rcParams['lines.linewidth'] = 2
# mpl.rcParams['lines.color'] = 'r' # color didn't change
mpl.rcParams['axes.prop_cycle'] = cycler(color='r')
plt.plot(data)

mpl.rc_file_defaults()

# %%
# the above can also be done in single line as follows
mpl.rc('lines', linewidth=4, color='r')
plt.plot(data)

# %%
# display where the currently active matplotlibrc file was loaded from
mpl.matplotlib_fname()


# %%
# mystyle
with plt.style.context(('mystyle')):
    plt.plot(np.sin(np.linspace(0, 2 * np.pi)), 'r-o')
plt.show()
