"""Plotting on data aware grids."""

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib as mpl
import matplotlib.pyplot as plt
sns.set(style="ticks")

# %%
tips = sns.load_dataset("tips")

# %%
# Facetgrid
g = sns.FacetGrid(tips, col="time")

# %%
g = sns.FacetGrid(tips, col="time")
g.map(plt.hist, "tip")

# %%
g = sns.FacetGrid(tips, col="sex", hue="smoker")
g.map(plt.scatter, "total_bill", "tip", alpha=.7)
g.add_legend()

# %%
g = sns.FacetGrid(tips, row="smoker", col="time", margin_titles=True)
g.map(sns.regplot, "size", "total_bill", color=".3", fit_reg=False, x_jitter=.1)

# %%
g = sns.FacetGrid(tips, col="day", size=4, aspect=.5, margin_titles=False)
g.map(sns.barplot, "sex", "total_bill")

# %%
titanic = sns.load_dataset("titanic")
titanic = titanic.assign(deck=titanic.deck.astype(object)).sort_values("deck")

# %%
# use of gridspec
g = sns.FacetGrid(titanic, col="class", sharex=False,
                  gridspec_kws={"width_ratios": [5, 3, 3]})
g.map(sns.boxplot, "deck", "age")

# %%
# specify an ordering of any facet dimension with the appropriate
# *_order parameter
ordered_days = tips.day.value_counts().index
g = sns.FacetGrid(tips, row="day", row_order=ordered_days, size=1.7, aspect=4)
g.map(sns.distplot, "total_bill", hist=False, rug=True)

# %%
# use a dictionary that maps the names of values in the hue variable to valid
# matplotlib colors:
pal = dict(Lunch="seagreen", Dinner="gray")
g = sns.FacetGrid(tips, hue="time", palette=pal, size=5)
g.map(plt.scatter, "total_bill", "tip", s=50, alpha=.7, linewidth=.5,
      edgecolor="white")
g.add_legend()

# %%
# use of hue_kws
g = sns.FacetGrid(tips, hue="sex", palette="Set1", size=5,
                  hue_kws={"marker": ["^", "v"]})
g.map(plt.scatter, "total_bill", "tip", s=100, linewidth=.5, edgecolor="white")
g.add_legend()

# %%
# colwrap
attend = sns.load_dataset("attention").query("subject <= 12")
g = sns.FacetGrid(attend, col="subject", col_wrap=4, size=2, ylim=(0, 10))
g.map(sns.pointplot, "solutions", "score", color=".3", ci=None)

# %%
with sns.axes_style("white"):
    g = sns.FacetGrid(tips, row="sex", col="smoker", margin_titles=True,
                      size=2.5)
g.map(plt.scatter, "total_bill", "tip", color="#334488", edgecolor="white",
      lw=.5)
g.set_axis_labels("Total bill (US Dollars)", "Tip")
g.set(xticks=[10, 30, 50], yticks=[2, 6, 10])
g.fig.subplots_adjust(wspace=.02, hspace=.02)

# %%
g = sns.FacetGrid(tips, col="smoker", margin_titles=True, size=4)
g.map(plt.scatter, "total_bill", "tip", color="#338844", edgecolor="white",
      s=50, lw=1)
for ax in g.axes.flat:
    ax.plot((0, 50), (0, .2 * 50), c=".2", ls="--")
g.set(xlim=(0, 60), ylim=(0, 14))

# %%
# Mapping custom functions onto the grid


def quantile_plot(x, **kwargs):
    """Quantile plot."""
    qntls, xr = stats.probplot(x, fit=False)
    plt.scatter(xr, qntls, **kwargs)


# %%
g = sns.FacetGrid(tips, col="sex", size=4)
g.map(quantile_plot, "total_bill")

# %%


def qqplot(x, y, **kwargs):
    """Bivariate plot."""
    _, xr = stats.probplot(x, fit=False)
    _, yr = stats.probplot(y, fit=False)
    plt.scatter(xr, yr, **kwargs)


# %%
g = sns.FacetGrid(tips, col="smoker", size=4)
g.map(qqplot, "total_bill", "tip")

# %%
g = sns.FacetGrid(tips, hue="time", col="sex", size=4)
g.map(qqplot, "total_bill", "tip")
g.add_legend()

# %%
g = sns.FacetGrid(tips, hue="time", col="sex", size=4,
                  hue_kws={"marker": ["s", "D"]})
g.map(qqplot, "total_bill", "tip", s=40, edgecolor="w")
g.add_legend()

# %%


def hexbin(x, y, color, **kwargs):
    """Hexbin."""
    cmap = sns.light_palette(color, as_cmap=True)
    plt.hexbin(x, y, gridsize=15, cmap=cmap, **kwargs)


with sns.axes_style("dark"):
    g = sns.FacetGrid(tips, hue="time", col="time", size=4)
g.map(hexbin, "total_bill", "tip", extent=[0, 50, 0, 10])

# %%
# Plotting pairwise relationships in a dataset
iris = sns.load_dataset("iris")
g = sns.PairGrid(iris)
g.map(plt.scatter)

# %%
# plot a different function on the diagonal
g = sns.PairGrid(iris)
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)

# %%
g = sns.PairGrid(iris, hue="species")
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)
g.add_legend()

# %%
# By default every numeric column in the dataset is used.
# But we can focus on particular relationshipself.
g = sns.PairGrid(iris, vars=["sepal_length", "sepal_width"], hue="species")
g.map(plt.scatter)

# %%
# use a different function in the upper and lower triangles
g = sns.PairGrid(iris)
g.map_upper(plt.scatter)
g.map_lower(sns.kdeplot, cmap="Blues_d")
g.map_diag(sns.kdeplot, lw=3, legend=False)

# %%
# plot with different variables in the rows and columns.
g = sns.PairGrid(tips, y_vars=["tip"], x_vars=["total_bill", "size"], size=4)
g.map(sns.regplot, color=".3")
g.set(ylim=(-1, 11), yticks=[0, 5, 10])

# %%
# use a different palette
g = sns.PairGrid(tips, hue="size", palette="GnBu_d")
g.map(plt.scatter, s=50, edgecolor="white")
g.add_legend()

# %%
# pairplot
sns.pairplot(iris, hue="species", size=2.5)
g = sns.pairplot(iris, hue="species", palette="Set2",
                 diag_kind="kde", size=2.5)
