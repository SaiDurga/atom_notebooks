"""Visualizing the distribution of a dataset."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
sns.set(color_codes=True)

# %%
# Plotting univariate distributions
x = np.random.normal(size=10)
sns.distplot(x)

# %%
# Histograms
x = np.random.normal(0, 1, size=30)
sns.kdeplot(x, shade=True)

# %%
# with different bandwidths
sns.kdeplot(x)
sns.kdeplot(x, bw=.2, label="bw: 0.2")
sns.kdeplot(x, bw=2, label="bw: 2")
plt.legend()

# %%
sns.kdeplot(x, shade=True, cut=1)
sns.rugplot(x)

# %%
# Fitting parametric distributions
x = np.random.gamma(6, size=200)
sns.distplot(x, kde=False, fit=stats.gamma)

# %%
# Plotting bivariate distributions
mean, cov = [0, 1], [(1, .5), (.5, 1)]
data = np.random.multivariate_normal(mean, cov, 200)
df = pd.DataFrame(data, columns=["x", "y"])

# %%
# scatterplots
sns.jointplot(x="x", y="y", data=df)

# %%
# hexbin plots
x, y = np.random.multivariate_normal(mean, cov, 1000).T
with sns.axes_style("white"):
    sns.jointplot(x=x, y=y, kind="hex", color="k")

# %%
# kernel density estimation
sns.jointplot(x="x", y="y", data=df, kind="kde")

# %%
f, ax = plt.subplots(figsize=(6, 6))
sns.kdeplot(df.x, df.y, ax=ax)
sns.rugplot(df.x, color="g", ax=ax)
sns.rugplot(df.y, vertical=True, ax=ax)

# %%
f, ax = plt.subplots(figsize=(6, 6))
cmap = sns.cubehelix_palette(as_cmap=True, dark=0, light=1, reverse=True)
sns.kdeplot(df.x, df.y, cmap=cmap, n_levels=60, shade=True)

# %%
g = sns.jointplot(x="x", y="y", data=df, kind="kde", color="m")
g.plot_joint(plt.scatter, c="w", s=30, linewidth=1, marker="+")
g.ax_joint.collections[0].set_alpha(0)
g.set_axis_labels("$X$", "$Y$")

# %%
# Visualizing pairwise relationships in a dataset
iris = sns.load_dataset("iris")
sns.pairplot(iris)

# %%
g = sns.PairGrid(iris)
g.map_diag(sns.kdeplot)
g.map_offdiag(sns.kdeplot, cmap="Blues_d", n_levels=6)
