"""Controlling figure aesthetics."""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# simple function to plot sine waves


def sinplot(flip=1):
    """Draw sinplot."""
    x = np.linspace(0, 14, 100)
    for i in range(1, 7):
        plt.plot(x, np.sin(x + i * .5) * (7 - i) * flip)


sinplot()

# %%
# to set seaborn style
sns.set()
sinplot()


# %%
# Seaborn figure styles
sns.set_style("whitegrid")
data = np.random.normal(size=(20, 6)) + np.arange(6) / 2
sns.boxplot(data=data)

# %%
sns.set_style("dark")
sinplot()

# %%
sns.set_style("white")
sinplot()

# %%
sns.set_style("darkgrid")
sinplot()

# %%
sns.set_style("ticks")
sinplot()

# %%
# Removing axes spines(white and ticks styles)
sinplot()
sns.despine()

# %%
sns.set_style("dark")
sinplot()
# sns.despine()

# %%
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine()

# %%
# trim and offset
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(trim=False, top=True, right=False)

# %%
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine()

f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(offset=10)

f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(offset=-10)

# %%
# Temporarily setting figure style
with sns.axes_style("darkgrid"):
    plt.subplot(211)
    sinplot()
plt.subplot(212)
sinplot()

# %%
with sns.axes_style("darkgrid"):
    plt.subplot(211)
    sinplot()
with sns.axes_style("whitegrid"):
    plt.subplot(212)
    sinplot(1)
    sns.despine()

# %%
# Overriding elements of the seaborn styles
sns.axes_style()
sns.set_style("darkgrid")
sns.axes_style()

# %%
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
sinplot()

# %%
# Resetting to defalut parameters
sns.axes_style()
sns.set()
sns.axes_style()

# %%
# Scaling plot elements
sns.set_context("paper")
sinplot()

# %%
sns.set_context("talk")
sinplot()

# %%
sns.set_context("poster")
sinplot()

# %%
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})
sinplot()
