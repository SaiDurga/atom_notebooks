"""Matplotlib tutorial."""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# %%
# simple plot
X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)
plt.plot(X, C)
plt.plot(X, S)
plt.show()

# %%
# Instantiating defaults
plt.figure(figsize=(8, 6))
plt.subplot(111)
plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-")
plt.plot(X, S, color="green", linewidth=1.0, linestyle="-")
plt.xlim(-4.0, 4.0)
plt.xticks(np.linspace(-4, 4, 9, endpoint=True))
plt.ylim(-1.0, 1.0)
plt.yticks(np.linspace(-1, 1, 5, endpoint=True))
plt.show()

# %%
# Changing colors and linewidths
plt.figure(figsize=(10, 6), dpi=80)
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-")

plt.figure(figsize=(10, 6))
plt.plot(X, C, color="green", linewidth=2.5, linestyle=":")
plt.plot(X, S, color="black",  linewidth=2.5, linestyle="-.")

# %%
# Setting limits
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-")
plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(C.min()*1.1, C.max()*1.1)

# %%
# Setting ticks
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-")
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])
plt.yticks([-1, 0, +1])

# %%
# Setting tick labels
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-")
(plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
 [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$']))
plt.yticks([-1, 0, +1], [r'$-1$', r'$0$', r'$+1$'])

# %%
# moving spines
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-", label="cosine")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-", label="sine")
(plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
 [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$']))
plt.yticks([-1, 0, +1], [r'$-1$', r'$0$', r'$+1$'])
ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# add legend
plt.legend(loc='upper left', frameon=False)

# --------------------------------------------------------------------
# Figures, Subplots, Axes and Ticks

# %%
# Regular Subplots
n = 256
X = np.linspace(-np.pi, np.pi, n, endpoint=True)
Y = np.sin(2*X)

plt.plot(X, Y+1, color='blue', alpha=1.00)
plt.fill_between(X, 1, Y+1, (Y+1) > 1, color='blue', alpha=.25)
plt.fill_between(X, 1, Y+1, (Y+1) < 1, color='blue',  alpha=.25)
plt.plot(X, Y-1, color='blue', alpha=1.00)
plt.fill_between(X, -1, Y-1, (Y-1) > -1, color='blue', alpha=.25)
plt.fill_between(X, -1, Y-1, (Y-1) < -1, color='red',  alpha=.25)
plt.show()

# %%
# scatter plot
n = 1024
X = np.random.normal(0, 1, n)
Y = np.random.normal(0, 1, n)
angle = np.arctan2(X, Y)
plt.scatter(X, Y, c=angle)
plt.xlim(-2, 2)
plt.show()

# %%
# Bar Plots
n = 12
X = np.arange(n)
Y1 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
Y2 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)

plt.bar(X, +Y1, facecolor='#9999ff', edgecolor='white')
plt.bar(X, -Y2, facecolor='#ff9999', edgecolor='white')
for x, y in zip(X, Y1):
    plt.text(x+0.4, y+0.05, '%.2f' % y, ha='center', va='bottom')
for x, y in zip(X, Y2):
    plt.text(x+0.4, -(y+0.05), '%.2f' % y, ha='center', va='top')

plt.ylim(-1.25, +1.25)

# %%
# contour plots


def f(x, y):
    """Function."""
    return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)


n = 256
x = np.linspace(-3, 3, n)
y = np.linspace(-3, 3, n)
X, Y = np.meshgrid(x, y)
plt.contourf(X, Y, f(X, Y), 8, alpha=.75, cmap='jet')
C = plt.contour(X, Y, f(X, Y), 8, colors='black', linewidth=.5)
plt.clabel(C)
plt.show()

# %%
# imshow
n = 10
x = np.linspace(-3, 3, 4*n)
y = np.linspace(-3, 3, 3*n)
X, Y = np.meshgrid(x, y)
plt.imshow(f(X, Y))
plt.set_cmap('Greys')
plt.colorbar()

# %%
# piecharts
n = 20
Z = np.random.uniform(0, 1, n)
plt.pie(Z)
plt.show()

# %%
# quiver plot
n = 8
X, Y = np.mgrid[0:n, 0:n]
plt.quiver(X, Y)
plt.quiver(X, Y, facecolor=None, edgecolor='k')
plt.show()

# %%
# grids
axes = plt.gca()
axes.set_xlim(0, 4)
axes.set_ylim(0, 3)
axes.set_xticklabels([])
axes.set_yticklabels([])

plt.show()

# %%
# multiplots
plt.subplot(2, 1, 1)
plt.subplot(2, 3, 4)
plt.subplot(2, 3, 5)
plt.subplot(2, 3, 6)
plt.show()

# %%
# polar plots
plt.axes([0, 0, 1, 1], polar=True)

N = 20
theta = np.arange(0.0, 2*np.pi, 2*np.pi/N)
radii = 10*np.random.rand(N)
width = np.pi/4*np.random.rand(N)
bars = plt.bar(theta, radii, width=width, bottom=0.0)

for r, bar in zip(radii, bars):
    bar.set_facecolor(plt.cm.jet(r/10.))
    bar.set_alpha(0.5)

plt.show()

# %%
# 3D plots
fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(-4, 4, 0.25)
Y = np.arange(-4, 4, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)

ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')
plt.contourf(X, Y, Z, zdir='Z', offset=-1.1, cmap='hot')
plt.show()
