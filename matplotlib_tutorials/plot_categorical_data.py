"""Plotting with categorical data."""

import pandas as pd
import seaborn as sns
sns.set(style="whitegrid", color_codes=True)

# %%
# loading datasets
titanic = sns.load_dataset("titanic")
tips = sns.load_dataset("tips")
iris = sns.load_dataset("iris")


# %%
data = (pd.DataFrame({'A': [2, 3, 1, 4, 5, 6],
        'B': ['b', 'b', 'a', 'b', 'a', 'a']}))
data
x = sns.stripplot(x="A", y="B", data=data)

# %%
sns.stripplot(x="day", y="total_bill", data=tips)

# %%
# to avoid overlapping of scatterplot points
sns.stripplot(x="day", y="total_bill", data=tips, jitter=True)

# %%
# Swarmplot
sns.swarmplot(x="day", y="total_bill", data=tips)

# %%
# add a nested categorical variable with the hue parameter.
sns.swarmplot(x="day", y="total_bill", hue="sex", data=tips)

# %%
# categories that look numerical will be sorted:
sns.swarmplot(x="size", y="total_bill", data=tips)

# %%
# categorical variable on the vertical axis
sns.swarmplot(x="total_bill", y="day", hue="time", data=tips)

# %%
# Distributions of observations within categories
sns.boxplot(x="day", y="total_bill", data=tips)

# %%
sns.boxplot(x="day", y="total_bill", hue="time", data=tips)

# %%
sns.boxplot(x="day", y="total_bill", hue="time", data=tips, dodge=False)

# %%
tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
sns.boxplot(x="day", y="total_bill", hue="weekend", data=tips, dodge=False)

# %%
# Violinplots
sns.violinplot(x="total_bill", y="day", data=tips)

# %%
sns.violinplot(x="total_bill", y="day", hue="time", data=tips)

# %%
sns.violinplot(x="total_bill", y="day", hue="time", data=tips,
               bw=.1, scale="count", scale_hue=False)

# %%
# split” the violins when the hue parameter has only two levels
sns.violinplot(x="day", y="total_bill", hue="sex", data=tips, split=True)

# %%
sns.violinplot(x="day", y="total_bill", hue="time", data=tips, split=True)

# %%
sns.violinplot(x="day", y="total_bill", hue="sex", data=tips,
               split=True, inner="stick", palette="Set3")

# %%
sns.violinplot(x="day", y="total_bill", hue="sex", data=tips,
               split=True, inner="box", palette="Set3")

# %%
sns.violinplot(x="day", y="total_bill", hue="sex", data=tips,
               split=True, inner="point", palette="Set3")

# %%
# It can also be useful to combine swarmplot() or swarmplot() with violinplot()
# or boxplot() to show each observation along with a summary of distribution
sns.violinplot(x="day", y="total_bill", data=tips, inner=None)
sns.swarmplot(x="day", y="total_bill", data=tips, color="w", alpha=.5)

# %%
sns.boxplot(x="day", y="total_bill", data=tips)
sns.swarmplot(x="day", y="total_bill", data=tips, color="w", alpha=.5)

# %%
# Statistical estimation within categories

x = sns.barplot(x='B', y="A", data=data)
x = sns.countplot(x='A', data=data)
x = sns.countplot(x='B', data=data)

# %%
sns.barplot(x="sex", y="survived", hue="class", data=titanic)

# %%
sns.countplot(x="deck", data=titanic, palette="Greens_d")

# %%
sns.countplot(y="deck", hue="class", data=titanic, palette="Greens_d")


# %%
# Point plots
sns.pointplot(x="sex", y="survived", hue="class", data=titanic)

# %%
sns.pointplot(x="class", y="survived", hue="sex", data=titanic,
              palette={"male": "g", "female": "m"},
              markers=["^", "o"], linestyles=["-", "--"])

# %%
# Plotting “wide-form” data
sns.boxplot(data=iris, orient="h")

# %%
sns.violinplot(x=iris.species, y=iris.sepal_length)

# %%
sns.countplot(y="deck", data=titanic, color="c")

# %%
# Drawing multi-panel categorical plots
sns.factorplot(x="day", y="total_bill", hue="smoker", data=tips)

# %%
sns.factorplot(x="day", y="total_bill", hue="smoker", data=tips, kind="bar")

# %%
sns.factorplot(x="day", y="total_bill", hue="smoker",
               col="time", data=tips, kind="swarm")

# %%
sns.factorplot(x="time", y="total_bill", hue="smoker",
               col="day", data=tips, kind="box", size=4, aspect=.5)

# %%
g = sns.PairGrid(tips, x_vars=["smoker", "time", "sex"],
                 y_vars=["total_bill", "tip"], aspect=.75, size=3.5)
g.map(sns.violinplot, palette="pastel")

# %%
g = sns.PairGrid(tips, x_vars=["smoker", "time", "sex"],
                 y_vars=["total_bill", "tip"], aspect=.75, size=3.5)
g.map(sns.pointplot, palette="pastel")

# %%
g = sns.PairGrid(tips, x_vars=["smoker", "time", "sex"],
                 y_vars=["total_bill", "tip"], aspect=.75, size=3.5)
g.map(sns.barplot, palette="pastel")
