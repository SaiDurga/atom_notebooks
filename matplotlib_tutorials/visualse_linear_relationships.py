"""Visualizing linear relationships."""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(color_codes=True)

# %%
tips = sns.load_dataset("tips")

# %%
# Functions to draw linear regression models
# regplot
sns.regplot(x="total_bill", y="tip", data=tips)

# %%
# lmplot
sns.lmplot(x="total_bill", y="tip", data=tips)

# %%
sns.regplot(x=tips['total_bill'], y=tips['tip'], data=tips)
# sns.lmplot(x=tips['total_bill'], y=tips['tip'], data=tips)
sns.regplot(x=tips['total_bill'], y=tips['tip'])

# %%
# we can fit a linear regression when one of the variables takes discretevalues
# the simple scatterplot produced by this kind of dataset is often not optimal
sns.lmplot(x="size", y="tip", data=tips)

# %%
# add some random noise (“jitter”) to the discrete values to make the
# distribution of those values more clear
sns.lmplot(x="size", y="tip", data=tips, x_jitter=.05)

# %%
# collapse over the observations in each discrete bin to plot an estimate of
# central tendency along with a confidence interval
sns.lmplot(x="size", y="tip", data=tips, x_estimator=np.mean)

# %%
# Fitting different kinds of models
anscombe = sns.load_dataset("anscombe")
anscombe

# %%
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
           ci=None, scatter_kws={"s": 80})

# %%
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
           ci=None, scatter_kws={"s": 80})
# linear regresiion is not a good fit

# %%
# fit a polynomial regression model to explore simple kinds of nonlinear trends
# in the dataset
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
           order=2, ci=None, scatter_kws={"s": 80})

# %%
# outliers
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           ci=None, scatter_kws={"s": 80})

# %%
# if outliers -- fit a robustic regression
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           robust=True, ci=None, scatter_kws={"s": 80})

# %%
# whwn y variable is binary
tips["big_tip"] = (tips.tip / tips.total_bill) > .15
sns.lmplot(x="total_bill", y="big_tip", data=tips, y_jitter=.03)

# %%
# use logistic regression for the above case
sns.lmplot(x="total_bill", y="big_tip", data=tips, logistic=True, y_jitter=.03)

# %%
# non parametric regresiion
sns.lmplot(x="total_bill", y="tip", data=tips, lowess=True)

# %%
# residplot() is used to find if linear model is appropriate or not
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
              scatter_kws={"s": 80})

# %%
# If there is structure in the residuals, simple linear regression is not good
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
              scatter_kws={"s": 80})

# %%
# Conditioning on other variables
# plot both levels on the same axes and to use color to distinguish them:
sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips)

# %%
# use different scatterplot markers to make plots
sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
           markers=["o", "x"], palette="Set1")

# %%
# To add another variable, draw multiple facets"
sns.lmplot(x="total_bill", y="tip", hue="smoker", col="time", data=tips)

# %%
sns.lmplot(x="total_bill", y="tip", hue="smoker", col="time", row="sex",
           data=tips)

# %%
# Controlling the size and shape of the plot
# for regplot(), create a figure object
f, ax = plt.subplots(figsize=(5, 6))
sns.regplot(x="total_bill", y="tip", data=tips, ax=ax)

# %%
# the size and shape of the lmplot() figure is controlled through the FacetGrid
# interface using the size and aspect parameters.
sns.lmplot(x="total_bill", y="tip", col="day", data=tips, col_wrap=2, size=3)

# %%
# use aspect
sns.lmplot(x="total_bill", y="tip", col="day", data=tips, aspect=.5)
