"""Matplotlib tutorial-2."""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter

# %%
# f you provide a single list or array to the plot() command,
# matplotlib assumes it is a sequence of y values, and automatically
# generates the x values for you.
plt.plot([1, 2, 3, 4])
plt.ylabel('some numbers')
plt.show()

# %%
# to plot x versus y, you can issue the command
plt.plot([1, 2, 3, 4], [1, 4, 9, 16])

# %%
# Formatting the style of the plot
# format string that indicates the color and line type of the plot
plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'ro')
plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'b-')
plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'r^')

# %%
# The axis() command itakes a list of [xmin, xmax, ymin, ymax]
# and specifies the viewport of the axes
plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'ro')
plt.axis([0, 5, 0, 20])
plt.show()

# %%
# plot several lines with different format styles in one command using arrays.
t = np.arange(0., 5., 0.2)
plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
plt.show()

# %%
# Plotting with keyword strings
data = {'a': np.arange(50),
        'c': np.random.randint(0, 50, 50),
        'd': np.random.randn(50)}
data['b'] = data['a'] + 10 * np.random.randn(50)
data['d'] = np.abs(data['d']) * 100

plt.scatter('a', 'b', c='d', s='c', data=data)
plt.xlabel('entry a')
plt.ylabel('entry b')
plt.show()

# %%
# Plotting with categorical variables
names = ['group_a', 'group_b', 'group_c']
values = [1, 10, 100]

plt.figure(1, figsize=(9, 3))

plt.subplot(131)
plt.bar(names, values)
plt.subplot(132)
plt.scatter(names, values)
plt.subplot(133)
plt.plot(names, values)
plt.suptitle('Categorical Plotting')
plt.show()

# %%
# Working with multiple figures and axes
n = np.arange(0, 10, 0.1)
y1 = np.cos(n)
y2 = np.sin(n)
plt.figure(figsize=(8, 6))
plt.subplot(2, 1, 1)
plt.plot(n, y1, 'b-')
plt.title('cos-wave')
plt.subplot(2, 1, 2)
plt.plot(n, y2, 'g-')
plt.title('sine-wave')
plt.show()


# %%
plt.figure()
plt.subplot(221)
plt.plot([1, 2, 3, 4])
plt.subplot(222)
plt.plot([-1, -2, -3, -4])
plt.subplot(223)
plt.plot([4, 3, 2, 1])
plt.subplot(224)
plt.plot([-4, -3, -2, -1])
plt.show()

# %%
# working with texts
mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

# the histogram of the data
plt.hist(x, 50, density=1, facecolor='g', alpha=0.75)
plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)
plt.text(120, .025, 'mu=100, sigma=15')
plt.show()

# %%
# annotate
ax = plt.subplot(111)

t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2*np.pi*t)
line = plt.plot(t, s, lw=2)
plt.annotate('local max', xy=(2, 1), xytext=(0.5, 1.5),
             arrowprops=dict(facecolor='blue', headwidth=25, shrink=0.04),
             )
plt.ylim(-2, 2)
plt.show()

# %%
# Logarithmic and other nonlinear axes
np.random.seed(19680801)
y = np.random.normal(loc=0.5, scale=0.4, size=1000)
y = y[(y > 0) & (y < 1)]
y.sort()
x = np.arange(len(y))
plt.figure(1)
# linear
plt.subplot(221)
plt.plot(x, y)
plt.yscale('linear')
plt.title('linear')
plt.grid(True)
# log
plt.subplot(222)
plt.plot(x, y)
plt.yscale('log')
plt.title('log')
plt.grid(True)
# symmetric log
plt.subplot(223)
plt.plot(x, y - y.mean())
plt.yscale('symlog', linthreshy=0.01)
plt.title('symlog')
plt.grid(True)
# logit
plt.subplot(224)
plt.plot(x, y)
plt.yscale('logit')
plt.title('logit')
plt.grid(True)
plt.gca().yaxis.set_minor_formatter(NullFormatter())
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.3,
                    wspace=0.35)

plt.show()


# %%
# controlling line properties
x = np.arange(1, 10)
y = x**2

# %%
# linewidth
plt.subplot(211)
plt.plot(x, y, linewidth=1)
plt.subplot(212)
plt.plot(x, y, linewidth=5)
