"""Matplotlib practice tutorial-4."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
# Figure with no axes
fig = plt.figure()
fig
fig = plt.plot([1, 2, 3, 4])
fig
fig.suptitle('No axes on this figure')

# %%
# A figure with a 2x2 grid of Axes
fig, ax_lst = plt.subplots(2, 2)

# %%
# types of inputs to plotting functions
# conver pandas dataframe to np.array
a = pd.DataFrame(np.random.rand(4, 5), columns=list('abcde'))
a
plt.plot(a)
plt.legend()
a_asndarray = a.values
a_asndarray
plt.plot(a_asndarray)

# %%
# convert np.matrix to np.a_asndarray
b = np.matrix([[1, 2], [3, 4]])
b
plt.plot(b)
b_asarray = np.asarray(b)
b_asarray
plt.plot(b_asarray)

# %%
# simple plot
x = np.linspace(0, 2, 100)
plt.plot(x, x, label='linear')
plt.plot(x, x**2, label='quadratic')
plt.plot(x, x**3, label='cubic')
plt.xlabel('x label')
plt.ylabel('y label')
plt.title('Simple plot')
plt.legend()
plt.show()

# %%
# coding styles
x = np.arange(0, 10, 0.2)
y = np.sin(x)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(x, y)
plt.show()

# %%
# function to plot same plots with different data


def my_plotter(ax, data1, data2, param_dict):
    """Plot using the data given."""
    return ax.plot(data1, data2, **param_dict)


data1, data2, data3, data4 = np.random.randn(4, 100)
fig, ax = plt.subplots(1, 1)
my_plotter(ax, data1, data2, {'marker': 'x'})

fig, (ax1, ax2) = plt.subplots(1, 2)
my_plotter(ax1, data1, data2, {'marker': 'x'})
my_plotter(ax2, data3, data4, {'marker': 'o'})
